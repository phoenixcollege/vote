<?php

namespace Tests\App\Traits;

use Smorken\Auth\Models\Eloquent\User;

trait Auth
{
    protected function mockAuth(
        $user_data = ['id' => 1, 'first_name' => 'foo', 'last_name' => 'bar', 'email' => 'foomail@example.org'],
        $acting_as = true
    ): \Smorken\Auth\Contracts\Models\User {
        $user = User::factory()->make($user_data);
        foreach ($user_data as $k => $v) {
            $user->$k = $v;
        }
        if ($acting_as && method_exists($this, 'actingAs')) {
            $this->actingAs($user);
        }

        return $user;
    }
}
