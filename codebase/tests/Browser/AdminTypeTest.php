<?php

namespace Tests\App\Browser;

use App\Models\Eloquent\Type;
use Laravel\Dusk\Browser;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Auth\Models\Eloquent\User;
use Tests\App\DuskTestCase;

class AdminTypeTest extends DuskTestCase
{
    #[Test]
    public function testBaseRouteWithAdminUser(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/type')
                ->assertSee('Type Administration')
                ->assertSee('Type 1')
                ->logout();
        });
    }

    #[Test]
    public function testBaseRouteWithoutAdminUser(): void
    {
        $user = User::factory()->create([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/type')
                ->assertSee("You don't have permission")
                ->logout();
        });
    }

    #[Test]
    public function testCreateNewRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/type')
                ->clickLink('New')
                ->assertPathIs('/admin/type/create')
                ->type('name', 'Test Create')
                ->select('category_id', '1')
                ->press('Save')
                ->assertPathIs('/admin/type')
                ->assertSee('Test Create')
                ->logout();
        });
    }

    #[Test]
    public function testCreateNewRecordFailsValidation(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/type')
                ->clickLink('New')
                ->assertPathIs('/admin/type/create')
                ->press('Save')
                ->assertPathIs('/admin/type/create')
                ->assertSee('name field is required')
                ->logout();
        });
    }

    #[Test]
    public function testDeleteExistingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Type::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/type')
                ->assertSee($model->name)
                ->clickLink($model->id)
                ->assertPathIs('/admin/type/view/'.$model->id)
                ->clickLink('Delete')
                ->assertPathIs('/admin/type/delete/'.$model->id)
                ->assertSee($model->name)
                ->press('Delete')
                ->assertPathIs('/admin/type')
                ->assertDontSee($model->name)
                ->logout();
        });
    }

    #[Test]
    public function testDeleteMissingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/type/delete/99')
                ->assertSee('The resource you were trying to reach')
                ->logout();
        });
    }

    #[Test]
    public function testUpdateExistingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Type::factory()->create(['category_id' => 1]);
            $browser->loginAs($user)
                ->visit('/admin/type')
                ->assertSee($model->name)
                ->clickLink($model->id)
                ->assertPathIs('/admin/type/view/'.$model->id)
                ->clickLink('Update')
                ->assertPathIs('/admin/type/update/'.$model->id)
                ->assertInputValue('name', $model->name)
                ->assertSelected('category_id', $model->category_id)
                ->type('name', 'Test Update')
                ->press('Save')
                ->assertPathIs('/admin/type')
                ->assertSee('Test Update')
                ->logout();
        });
    }

    #[Test]
    public function testUpdateExistingRecordFailsValidation(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Type::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/type')
                ->assertSee($model->name)
                ->clickLink($model->id)
                ->assertPathIs('/admin/type/view/'.$model->id)
                ->clickLink('Update')
                ->assertPathIs('/admin/type/update/'.$model->id)
                ->assertInputValue('name', $model->name)
                ->clear('name')
                ->press('Save')
                ->assertPathIs('/admin/type/update/'.$model->id)
                ->assertSee('name field is required')
                ->logout();
        });
    }

    #[Test]
    public function testUpdateMissingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Type::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/type/update/99')
                ->assertSee('The resource you were trying to reach')
                ->logout();
        });
    }

    #[Test]
    public function testViewExistingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Type::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/type')
                ->assertSee($model->name)
                ->clickLink($model->id)
                ->assertPathIs('/admin/type/view/'.$model->id)
                ->assertSee($model->name)
                ->logout();
        });
    }

    #[Test]
    public function testViewMissingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/type/view/99')
                ->assertSee('The resource you were trying to reach')
                ->logout();
        });
    }
}
