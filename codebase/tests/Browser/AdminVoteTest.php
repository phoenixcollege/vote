<?php

namespace Tests\App\Browser;

use App\Models\Eloquent\Block;
use App\Models\Eloquent\Candidate;
use App\Models\Eloquent\Vote;
use Laravel\Dusk\Browser;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Auth\Models\Eloquent\User;
use Tests\App\DuskTestCase;

class AdminVoteTest extends DuskTestCase
{
    #[Test]
    public function testBaseRouteWithAdminUser(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/vote')
                ->assertSee('Vote Administration')
                ->assertSee('No records found')
                ->logout();
        });
    }

    #[Test]
    public function testBaseRouteWithoutAdminUser(): void
    {
        $user = User::factory()->create([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/vote')
                ->assertSee("You don't have permission")
                ->logout();
        });
    }

    #[Test]
    public function testDeleteExistingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $block = Block::factory()->create();
            $candidate = Candidate::factory()->create(['block_id' => $block->id]);
            $model = Vote::factory()->create(['candidate_id' => $candidate->id]);
            $browser->loginAs($user)
                ->visit('/admin/vote')
                ->assertSee($candidate->name)
                ->clickLink('delete')
                ->assertPathIs('/admin/vote/delete/'.$model->id)
                ->press('Delete')
                ->assertPathIs('/admin/vote')
                ->assertDontSee($candidate->name)
                ->logout();
        });
    }

    #[Test]
    public function testFilterByBlock(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $block = Block::factory()->create();
            $candidate = Candidate::factory()->create(['block_id' => $block->id]);
            $model = Vote::factory()->create(['candidate_id' => $candidate->id]);
            $block2 = Block::factory()->create();
            $candidate2 = Candidate::factory()->create(['block_id' => $block2->id]);
            $model2 = Vote::factory()->create(['candidate_id' => $candidate2->id]);
            $browser->loginAs($user)
                ->visit('/admin/vote')
                ->assertSee($candidate->name)
                ->assertSee($candidate2->name)
                ->select('block_id', $block2->id)
                ->press('Filter')
                ->assertPathIs('/admin/vote')
                ->assertSee($candidate2->name)
                ->assertDontSee($candidate->name)
                ->logout();
        });
    }

    #[Test]
    public function testFilterAggregate(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $block = Block::factory()->create();
            $candidate = Candidate::factory()->create(['block_id' => $block->id]);
            Vote::factory(3)->create(['candidate_id' => $candidate->id]);
            $block2 = Block::factory()->create();
            $candidate2 = Candidate::factory()->create(['block_id' => $block2->id]);
            Vote::factory(5)->create(['candidate_id' => $candidate2->id]);
            $browser->loginAs($user)
                ->visit('/admin/vote')
                ->assertSee($candidate->name)
                ->assertSee($candidate2->name)
                ->check('aggregate')
                ->press('Filter')
                ->assertPathIs('/admin/vote')
                ->assertSourceHas('<td>3</td>')
                ->assertSourceHas('<td>5</td>')
                ->logout();
        });
    }

    #[Test]
    public function testDeleteMissingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/vote/delete/99')
                ->assertSee('The resource you were trying to reach')
                ->logout();
        });
    }
}
