<?php

namespace Tests\App\Browser;

use App\Models\Eloquent\Category;
use Laravel\Dusk\Browser;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Auth\Models\Eloquent\User;
use Tests\App\DuskTestCase;

class AdminCategoryTest extends DuskTestCase
{
    #[Test]
    public function testBaseRouteWithAdminUser(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/category')
                ->assertSee('Category Administration')
                ->assertSee('Test Category')
                ->logout();
        });
    }

    #[Test]
    public function testBaseRouteWithoutAdminUser(): void
    {
        $user = User::factory()->create([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/category')
                ->assertSee("You don't have permission")
                ->logout();
        });
    }

    #[Test]
    public function testCreateNewRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/category')
                ->clickLink('New')
                ->assertPathIs('/admin/category/create')
                ->type('name', 'Test Create')
                ->press('Save')
                ->assertPathIs('/admin/category')
                ->assertSee('Test Create')
                ->logout();
        });
    }

    #[Test]
    public function testCreateNewRecordFailsValidation(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/category')
                ->clickLink('New')
                ->assertPathIs('/admin/category/create')
                ->press('Save')
                ->assertPathIs('/admin/category/create')
                ->assertSee('name field is required')
                ->logout();
        });
    }

    #[Test]
    public function testDeleteExistingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Category::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/category')
                ->assertSee($model->name)
                ->clickLink($model->id)
                ->assertPathIs('/admin/category/view/'.$model->id)
                ->clickLink('Delete')
                ->assertPathIs('/admin/category/delete/'.$model->id)
                ->assertSee($model->name)
                ->press('Delete')
                ->assertPathIs('/admin/category')
                ->assertDontSee($model->name)
                ->logout();
        });
    }

    #[Test]
    public function testDeleteMissingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/category/delete/99')
                ->assertSee('The resource you were trying to reach')
                ->logout();
        });
    }

    #[Test]
    public function testUpdateExistingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Category::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/category')
                ->assertSee($model->name)
                ->clickLink($model->id)
                ->assertPathIs('/admin/category/view/'.$model->id)
                ->clickLink('Update')
                ->assertPathIs('/admin/category/update/'.$model->id)
                ->assertInputValue('name', $model->name)
                ->type('name', 'Test Update')
                ->press('Save')
                ->assertPathIs('/admin/category')
                ->assertSee('Test Update')
                ->logout();
        });
    }

    #[Test]
    public function testUpdateExistingRecordFailsValidation(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Category::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/category')
                ->assertSee($model->name)
                ->clickLink($model->id)
                ->assertPathIs('/admin/category/view/'.$model->id)
                ->clickLink('Update')
                ->assertInputValue('name', $model->name)
                ->clear('name')
                ->press('Save')
                ->assertPathIs('/admin/category/update/'.$model->id)
                ->assertSee('name field is required')
                ->logout();
        });
    }

    #[Test]
    public function testUpdateMissingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Category::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/category/update/99')
                ->assertSee('The resource you were trying to reach')
                ->logout();
        });
    }

    #[Test]
    public function testViewExistingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Category::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/category')
                ->assertSee($model->name)
                ->clickLink($model->id)
                ->assertPathIs('/admin/category/view/'.$model->id)
                ->assertSee($model->name)
                ->logout();
        });
    }

    #[Test]
    public function testViewMissingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/category/view/99')
                ->assertSee('The resource you were trying to reach')
                ->logout();
        });
    }
}
