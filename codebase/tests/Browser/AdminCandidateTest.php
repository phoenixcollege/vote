<?php

namespace Tests\App\Browser;

use App\Models\Eloquent\Block;
use App\Models\Eloquent\Category;
use App\Models\Eloquent\Type;
use Laravel\Dusk\Browser;
use Smorken\Auth\Models\Eloquent\User;
use Tests\App\DuskTestCase;

class AdminCandidateTest extends DuskTestCase
{
    public function testCreateCandidate(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $category = Category::factory()->create();
            Type::factory()->create(['category_id' => $category->id]);
            Block::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/candidate')
                ->clickLink('New')
                ->assertPathIs('/admin/candidate/create')
                ->type('name', 'Foo Bar')
                ->type('title', 'My Description')
                ->type('bio', 'My bio')
                ->attach('photo', __DIR__.'/generic_person.png')
                ->press('Save')
                ->assertPathIs('/admin/candidate')
                ->assertSee('Foo Bar')
                ->logout();
        });
    }
}
