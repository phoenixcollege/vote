<?php

namespace Tests\App\Browser;

use App\Contracts\Models\Block;
use Facebook\WebDriver\WebDriverBy;
use Laravel\Dusk\Browser;
use Smorken\Auth\Models\Eloquent\User;
use Tests\App\DuskTestCase;

class VoteTest extends DuskTestCase
{
    public function testBlockWithCandidates(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $block = $this->getCurrentBlock();
            $candidates = $this->addCandidatesToBlock($block);
            $browser->loginAs($user)
                ->visit('/vote/'.$block->id)
                ->assertSee($candidates[0]->name)
                ->assertSee($candidates[1]->name)
                ->assertSee($candidates[2]->name)
                ->logout();
        });
    }

    public function testBlockWithNoCandidates(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $block = $this->getCurrentBlock();
            $browser->loginAs($user)
                ->visit('/vote/'.$block->id)
                ->assertSee('No candidates found')
                ->logout();
        });
    }

    public function testClosedBlockWithCandidatesCannotVote(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $block = $this->getCurrentBlock([
                'start' => date('Y-m-d H:i:s', strtotime('-1 week')),
                'end' => date('Y-m-d H:i:s', strtotime('- 1 day')),
            ]);
            $candidates = $this->addCandidatesToBlock($block);
            $browser->loginAs($user)
                ->visit('/vote/'.$block->id)
                ->assertSee('(closed)')
                ->assertSee($candidates[0]->name)
                ->assertSee($candidates[1]->name)
                ->assertSee($candidates[2]->name)
                ->assertMissing('#vote-1-'.$candidates[0]->id)
                ->assertMissing('#vote-1-'.$candidates[1]->id)
                ->assertMissing('#vote-1-'.$candidates[2]->id)
                ->logout();
        });
    }

    public function testInvalidBlock(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/vote/999')
                ->assertSee("I don't know where to find that")
                ->logout();
        });
    }

    public function testNotAuthRedirectsToLogin(): void
    {
        $this->browse(function (Browser $browser) {
            $block = $this->getCurrentBlock();
            $browser->visit('/vote/'.$block->id)
                ->assertPathIs('/login');
        });
    }

    public function testVotingSetsCandidateRadioButton(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $block = $this->getCurrentBlock();
            $candidates = $this->addCandidatesToBlock($block);
            $browser->loginAs($user)
                ->visit('/vote/'.$block->id)
                ->assertSee($candidates[0]->name)
                ->assertSee($candidates[1]->name)
                ->assertSee($candidates[2]->name);
            $radio = $browser->driver->findElement(WebDriverBy::id('vote-1-'.$candidates[1]->id));
            $radio->click();
            $saveButton = $browser->driver->findElement(WebDriverBy::xpath("//button[contains(.,'Save')]"));
            $saveButton->submit();
            $browser->assertPathIs('/')
                ->visit('/vote/'.$block->id)
                ->assertRadioSelected('#vote-1-'.$candidates[1]->id, $candidates[1]->id)
                ->logout();
        });
    }

    protected function addCandidatesToBlock($block, $type_id = 1, $count = 3): array
    {
        $candidates = [];
        for ($i = 0; $i < $count; $i++) {
            $candidates[] = \App\Models\Eloquent\Candidate::factory()->create([
                'block_id' => $block->id,
                'type_id' => $type_id,
            ]);
        }

        return $candidates;
    }

    protected function getCurrentBlock($attrs = []): Block
    {
        $default = [
            'start' => date('Y-m-d H:i:s', strtotime('-1 day')),
            'end' => date('Y-m-d H:i:s', strtotime('+1 day')),
            'category_id' => 1,
            'private' => 0,
        ];
        $new = $default;
        foreach ($attrs as $k => $v) {
            $new[$k] = $v;
        }

        return \App\Models\Eloquent\Block::factory()->create($new);
    }
}
