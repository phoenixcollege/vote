<?php

namespace Tests\App\Browser;

use Laravel\Dusk\Browser;
use Smorken\Auth\Models\Eloquent\User;
use Tests\App\DuskTestCase;

class HomeTest extends DuskTestCase
{
    public function testAuthNoBlocksAvailable(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/')
                ->assertDontSee('You must log in to vote')
                ->assertSee('no voting blocks')
                ->logout();
        });
    }

    public function testAuthWithBlocksAvailableAndPrivateHidesChart(): void
    {
        $b = \App\Models\Eloquent\Block::factory()->create([
            'name' => 'Foo Block',
            'start' => date('Y-m-d H:i:s', strtotime('-1 day')),
            'end' => date('Y-m-d H:i:s', strtotime('+1 day')),
            'category_id' => 1,
            'private' => 1,
        ]);
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/')
                ->assertDontSee('You must log in to vote')
                ->assertDontSee('no voting blocks')
                ->assertSee('Foo Block')
                ->assertSourceMissing('<h5 class="card-title">Foo Block</h5>')
                ->logout();
        });
    }

    public function testAuthWithBlocksAvailableChartable(): void
    {
        $b = \App\Models\Eloquent\Block::factory()->create([
            'name' => 'Foo Block',
            'start' => date('Y-m-d H:i:s', strtotime('-1 day')),
            'end' => date('Y-m-d H:i:s', strtotime('+1 day')),
            'category_id' => 1,
            'private' => 0,
        ]);
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/')
                ->assertDontSee('You must log in to vote')
                ->assertDontSee('no voting blocks')
                ->assertSee('Foo Block')
                ->assertSourceHas('<h5 class="card-title">Foo Block</h5>')
                ->logout();
        });
    }

    public function testAuthWithBlocksAvailableChartableWithData(): void
    {
        $b = \App\Models\Eloquent\Block::factory()->create([
            'name' => 'Foo Block',
            'start' => date('Y-m-d H:i:s', strtotime('-1 day')),
            'end' => date('Y-m-d H:i:s', strtotime('+1 day')),
            'category_id' => 1,
            'private' => 0,
        ]);
        $c = \App\Models\Eloquent\Candidate::factory()->create([
            'name' => 'Candidate 1',
            'block_id' => $b->id,
            'type_id' => 1,
        ]);
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/')
                ->assertDontSee('You must log in to vote')
                ->assertDontSee('no voting blocks')
                ->assertSee('Foo Block')
                ->assertSourceHas('<h5 class="card-title">Foo Block</h5>')
                ->assertSourceHas('<td>Candidate 1</td>')
                ->logout();
        });
    }

    public function testNotAuthRequestsLogin(): void
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->assertSee('You must log in to vote')
                ->assertDontSee('Pick one');
        });
    }
}
