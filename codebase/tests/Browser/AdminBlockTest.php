<?php

namespace Tests\App\Browser;

use App\Models\Eloquent\Block;
use App\Models\Eloquent\Candidate;
use App\Models\Eloquent\Vote;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverKeys;
use Laravel\Dusk\Browser;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Auth\Models\Eloquent\User;
use Tests\App\DuskTestCase;

class AdminBlockTest extends DuskTestCase
{
    #[Test]
    public function testBaseRouteWithAdminUser(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/block')
                ->assertSee('Block Administration')
                ->assertSee('No records found')
                ->logout();
        });
    }

    #[Test]
    public function testBaseRouteWithoutAdminUser(): void
    {
        $user = User::factory()->create([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/block')
                ->assertSee("You don't have permission")
                ->logout();
        });
    }

    #[Test]
    public function testCreateNewRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/block')
                ->clickLink('New')
                ->assertPathIs('/admin/block/create')
                ->type('name', 'Test Create');
            $this->setDateTimeFor($browser, 'start', date('mdY'), date('hiA'));
            $this->setDateTimeFor($browser, 'end', date('mdY', strtotime('+1 day')),
                date('hiA', strtotime('+1 day')));
            $browser->select('category_id', '1')
                ->press('Save')
                ->assertPathIs('/admin/block')
                ->assertSee('Test Create')
                ->logout();
        });
    }

    #[Test]
    public function testCreateNewRecordFailsValidation(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/block')
                ->clickLink('New')
                ->assertPathIs('/admin/block/create')
                ->press('Save')
                ->assertPathIs('/admin/block/create')
                ->assertSee('name field is required')
                ->assertSee('start field is required')
                ->assertSee('end field is required')
                ->assertSee('category id field is required')
                ->logout();
        });
    }

    #[Test]
    public function testDeleteExistingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Block::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/block')
                ->assertSee($model->name)
                ->clickLink('delete')
                ->assertPathIs('/admin/block/delete/'.$model->id)
                ->assertSee($model->name)
                ->press('Delete')
                ->assertPathIs('/admin/block')
                ->assertDontSee($model->name)
                ->logout();
        });
    }

    #[Test]
    public function testDeleteMissingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/block/delete/2')
                ->assertSee('The resource you were trying to reach')
                ->logout();
        });
    }

    #[Test]
    public function testUpdateExistingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Block::factory()->create(['category_id' => 1]);
            $browser->loginAs($user)
                ->visit('/admin/block')
                ->assertSee($model->name)
                ->clickLink('update')
                ->assertPathIs('/admin/block/update/'.$model->id)
                ->assertInputValue('name', $model->name)
                ->assertInputValue('start', $model->start->format('Y-m-d\TH:i'))
                ->assertInputValue('end', $model->end->format('Y-m-d\TH:i'))
                ->assertSelected('category_id', $model->category_id)
                ->type('name', 'Test Update')
                ->press('Save')
                ->assertPathIs('/admin/block')
                ->assertSee('Test Update')
                ->logout();
        });
    }

    #[Test]
    public function testUpdateExistingRecordFailsValidation(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Block::factory()->create(['category_id' => 1]);
            $browser->loginAs($user)
                ->visit('/admin/block')
                ->assertSee($model->name)
                ->clickLink('update')
                ->assertPathIs('/admin/block/update/'.$model->id)
                ->assertInputValue('name', $model->name)
                ->clear('name')
                ->press('Save')
                ->assertPathIs('/admin/block/update/'.$model->id)
                ->assertSee('name field is required')
                ->logout();
        });
    }

    #[Test]
    public function testUpdateMissingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Block::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/block/update/2')
                ->assertSee('The resource you were trying to reach')
                ->logout();
        });
    }

    #[Test]
    public function testViewExistingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Block::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/block')
                ->assertSee($model->name)
                ->clickLink($model->id)
                ->assertPathIs('/admin/block/view/'.$model->id)
                ->assertSee($model->name)
                ->logout();
        });
    }

    #[Test]
    public function testViewExistingRecordWithVotes(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Block::factory()->create();
            $candidate1 = Candidate::factory()->create(['block_id' => $model->id, 'type_id' => 1]);
            $candidate2 = Candidate::factory()->create(['block_id' => $model->id, 'type_id' => 1]);
            Vote::factory(2)->create(['candidate_id' => $candidate1->id]);
            Vote::factory()->create(['candidate_id' => $candidate2->id]);
            $browser->loginAs($user)
                ->visit('/admin/block')
                ->assertSee($model->name)
                ->clickLink($model->id)
                ->assertPathIs('/admin/block/view/'.$model->id)
                ->assertSee($model->name)
                ->assertSourceHas('<td>3</td>')
                ->logout();
        });
    }

    #[Test]
    public function testViewMissingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/block/view/2')
                ->assertSee('The resource you were trying to reach')
                ->logout();
        });
    }

    protected function setDateTimeFor(Browser $browser, string $id, string $date, string $time): void
    {
        $input = $browser->driver->findElement(WebDriverBy::id($id));
        $input->sendKeys($date);
        $input->sendKeys(WebDriverKeys::TAB);
        $input->sendKeys($time);
    }
}
