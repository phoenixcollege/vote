<?php

namespace Tests\App\Feature;

use App\Models\Eloquent\Block;
use App\Models\Eloquent\Candidate;
use App\Models\Eloquent\Vote;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\App\TestCase;

class AdminVoteTest extends TestCase
{
    use RefreshDatabase, WithoutMiddleware;

    /**
     * A basic test example.
     */
    public function testExportTest(): void
    {
        $block = Block::factory()->create();
        $candidate = Candidate::factory()->create(['block_id' => $block->id]);
        $votes = Vote::factory(3)->create(['candidate_id' => $candidate->id]);
        $response = $this->get('/admin/vote/export');

        $response->assertStatus(200);
        $header = "block_id,block,candidate_id,candidate,type_id,type,voter,vote_date\n";
        $response->assertSee($header);
        $line = sprintf('%d,"%s",%d,"%s",%d,%d,%s', $block->id, $block->name, $candidate->id, $candidate->name,
            $candidate->type_id, $candidate->type_id, $votes[0]->user_id);
        $response->assertSee($line, false);
    }
}
