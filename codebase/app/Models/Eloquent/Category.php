<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/12/15
 * Time: 10:45 AM
 */

namespace App\Models\Eloquent;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Smorken\Model\Concerns\WithDefaultScopes;
use Smorken\Model\Concerns\WithFriendlyKeys;

class Category extends Base implements \App\Contracts\Models\Category
{
    use WithDefaultScopes, WithFriendlyKeys;

    protected $fillable = [
        'name',
    ];

    protected array $friendlyKeys = [
        'id' => 'ID',
        'name' => 'Name',
        'created_at' => 'Created',
        'updated_at' => 'Updated',
    ];

    public function __toString(): string
    {
        return sprintf('%s', $this->name);
    }

    public function scopeDefaultOrder(Builder $query): Builder
    {
        return $query->orderBy('name');
    }

    public function types(): HasMany
    {
        return $this->hasMany(Type::class);
    }
}
