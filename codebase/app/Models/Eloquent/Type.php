<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/12/15
 * Time: 10:45 AM
 */

namespace App\Models\Eloquent;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Smorken\Model\Concerns\WithFriendlyKeys;

class Type extends Base implements \App\Contracts\Models\Type
{
    use WithFriendlyKeys;

    protected $fillable = [
        'name',
        'category_id',
    ];

    protected array $friendlyKeys = [
        'id' => 'ID',
        'name' => 'Name',
        'category_id' => 'Category ID',
        'created_at' => 'Created',
        'updated_at' => 'Updated',
    ];

    public function __toString(): string
    {
        return sprintf('%s', $this->name);
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function scopeCategoryIdIs(Builder $query, int $id): Builder
    {
        return $query->where('category_id', '=', $id);
    }

    public function scopeDefaultOrder(Builder $query): Builder
    {
        return $this->scopeOrderDefault($query);
    }

    public function scopeDefaultWiths(Builder $query): Builder
    {
        return $query->with('category');
    }

    public function scopeOrderDefault(Builder $query): Builder
    {
        return $query->orderBy('category_id')
            ->orderBy('name');
    }
}
