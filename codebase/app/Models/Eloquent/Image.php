<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/28/16
 * Time: 7:58 AM
 */

namespace App\Models\Eloquent;

use Smorken\Model\Concerns\WithDefaultScopes;

class Image extends Base implements \App\Contracts\Models\Image
{
    use WithDefaultScopes;

    protected $fillable = [
        'mime',
        'size',
        'data',
        'candidate_id',
        'default',
    ];

    public function __toString(): string
    {
        return sprintf('%s.jpg', $this->id);
    }
}
