<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/12/15
 * Time: 10:45 AM
 */

namespace App\Models\Eloquent;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Smorken\Model\Concerns\WithFriendlyKeys;

class Block extends Base implements \App\Contracts\Models\Block
{
    use WithFriendlyKeys;

    protected $casts = ['start' => 'datetime', 'end' => 'datetime'];

    protected $fillable = [
        'name',
        'start',
        'end',
        'category_id',
        'private',
    ];

    protected array $friendlyKeys = [
        'id' => 'ID',
        'name' => 'Block',
        'start' => 'Start Date',
        'end' => 'End Date',
        'category_id' => 'Category ID',
        'private' => 'Hidden',
        'created_at' => 'Created',
        'updated_at' => 'Updated',
    ];

    public function __toString(): string
    {
        return sprintf('%s (%s - %s)', $this->name, $this->start, $this->end);
    }

    public function candidates(): HasMany
    {
        return $this->hasMany(Candidate::class)
            ->defaultOrder();
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function currentActive(): ?\App\Contracts\Models\Block
    {
        return $this->newQuery()
            ->where('start', '<=', date('Y-m-d H:i:s'))
            ->where('end', '>=', date('Y-m-d H:i:s'))
            ->first();
    }

    public function getCountForCandidates(): Collection
    {
        return DB::table('candidates')
            ->select(DB::raw('COUNT(votes.candidate_id) as candidate_count,
                            candidates.name as candidate_name,
                            blocks.name as block_name,
                            types.name as type_name,
                            blocks.id as block_id,
                            types.id as type_id'))
            ->leftJoin('votes', 'candidates.id', '=', 'votes.candidate_id')
            ->join('blocks', 'candidates.block_id', '=', 'blocks.id')
            ->join('types', 'candidates.type_id', '=', 'types.id')
            ->where('candidates.block_id', $this->id)
            ->groupBy('candidates.id')
            ->get();
    }

    public function isOpen(): bool
    {
        $now = time();

        return strtotime($this->start) < $now && strtotime($this->end) > $now;
    }

    public function lastActive(): ?\App\Contracts\Models\Block
    {
        return $this->newQuery()
            ->where('end', '<=', date('Y-m-d H:i:s'))
            ->orderBy('end', 'DESC')
            ->first();
    }

    public function scopeCategoryIdIs(Builder $query, int $id): Builder
    {
        return $query->where('category_id', '=', $id);
    }

    public function scopeDefaultOrder(Builder $query): Builder
    {
        return $query->orderBy('start', 'desc');
    }

    public function scopeDefaultWiths(Builder $query): Builder
    {
        return $query->with('category');
    }

    public function scopeIdIs(Builder $query, string|int|array $id): Builder
    {
        if ($id) {
            $query->where('id', '=', $id);
        }

        return $query;
    }

    public function setEnd(string $value): void
    {
        $this->attributes['end'] = \Carbon\Carbon::createFromTimestamp(strtotime($value));
    }

    public function setStart(string $value): void
    {
        $this->attributes['start'] = \Carbon\Carbon::createFromTimestamp(strtotime($value));
    }

    public function toDateTimeLocalFormat(string $attribute): ?string
    {
        $value = $this->getAttribute($attribute);
        if ($value instanceof Carbon) {
            return $value->format('Y-m-d\TH:i');
        }

        return $value;
    }
}
