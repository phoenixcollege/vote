<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/12/15
 * Time: 10:45 AM
 */

namespace App\Models\Eloquent;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Candidate extends Base implements \App\Contracts\Models\Candidate
{
    protected $fillable = [
        'name',
        'block_id',
        'type_id',
        'title',
        'bio',
    ];

    public function __toString(): string
    {
        return sprintf('%s (%s)', $this->name, $this->block);
    }

    public function block(): BelongsTo
    {
        return $this->belongsTo(Block::class);
    }

    public function image(): HasOne
    {
        return $this->hasOne(Image::class)
            ->where('default', '=', 1)
            ->orderBy('updated_at', 'desc');
    }

    public function images(): HasMany
    {
        return $this->hasMany(Image::class)
            ->orderBy('default', 'DESC');
    }

    public function scopeBlockIdIs(Builder $query, int $id): Builder
    {
        return $query->where('block_id', '=', $id);
    }

    public function scopeDefaultOrder(Builder $query): Builder
    {
        return $this->scopeOrderDefault($query);
    }

    public function scopeDefaultWiths(Builder $query): Builder
    {
        return $query->with('block')
            ->with('type');
    }

    public function scopeOrderDefault(Builder $query): Builder
    {
        return $query->orderBy('block_id', 'desc')
            ->orderBy('type_id')
            ->orderBy('name');
    }

    public function scopeTypeIdIs(Builder $query, int $id): Builder
    {
        return $query->where('type_id', '=', $id);
    }

    public function type(): BelongsTo
    {
        return $this->belongsTo(Type::class);
    }

    public function voteCount(): int
    {
        return count($this->votes);
    }

    public function votes(): HasMany
    {
        return $this->hasMany(Vote::class);
    }
}
