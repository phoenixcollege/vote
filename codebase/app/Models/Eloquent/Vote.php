<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/12/15
 * Time: 10:45 AM
 */

namespace App\Models\Eloquent;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;

class Vote extends Base implements \App\Contracts\Models\Vote
{
    protected $fillable = [
        'user_id',
        'candidate_id',
    ];

    protected ?string $secret = null;

    public function __toString(): string
    {
        return sprintf('%s (%s)', $this->user_id, $this->candidate_id);
    }

    public function candidate(): BelongsTo
    {
        return $this->belongsTo(\App\Models\Eloquent\Candidate::class);
    }

    public function getUserIdAttribute(): string
    {
        return hash('tiger128,3', Arr::get($this->attributes, 'user_id').$this->getSecret());
    }

    public function scopeByUserId(Builder $query, int $user_id): Builder
    {
        $query->where('user_id', '=', $user_id);

        return $query;
    }

    public function scopeDefaultOrder(Builder $query): Builder
    {
        return $this->scopeOrderDefault($query);
    }

    public function scopeDefaultWiths(Builder $query): Builder
    {
        return $query->with(['candidate', 'candidate.block', 'candidate.type', 'candidate.votes']);
    }

    public function scopeHasBlockId(Builder $query, int $block_id): Builder
    {
        return $query->whereHas('candidate', function ($q) use ($block_id) {
            $q->where('block_id', '=', $block_id);
        });
    }

    public function scopeOrderDefault(Builder $query): Builder
    {
        return $query->select('votes.*')
            ->join('candidates', 'votes.candidate_id', '=', 'candidates.id')
            ->orderBy('candidates.block_id', 'desc')
            ->orderBy('candidates.type_id')
            ->orderBy('candidate_id');
    }

    protected function getSecret(): string
    {
        if (! $this->secret) {
            $this->secret = Config::get('app.key', 'some secret');
        }

        return $this->secret;
    }
}
