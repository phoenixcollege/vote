<?php

namespace App\Models\Eloquent;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Smorken\Model\Eloquent;

abstract class Base extends Eloquent
{
    use HasFactory;
}
