<?php

namespace App\Providers;

use App\Http\ViewComposers\Blocks;
use App\Http\ViewComposers\BlocksActiveAndUpcoming;
use App\Http\ViewComposers\Categories;
use App\Http\ViewComposers\Sanitize;
use App\Http\ViewComposers\Types;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    protected array $viewComposers = [
        'vote.index' => [
            Sanitize::class,
        ],
        'vote.candidate' => [
            Sanitize::class,
        ],
        'admin.candidate.create' => [
            BlocksActiveAndUpcoming::class,
            Types::class,
        ],
        'admin.candidate.index' => [
            Blocks::class,
            Types::class,
        ],
        'admin.candidate.update' => [
            BlocksActiveAndUpcoming::class,
            Types::class,
        ],
        'admin.candidate.view' => [
            Sanitize::class,
        ],
        'admin.block.create' => [
            Categories::class,
        ],
        'admin.block.index' => [
            Categories::class,
        ],
        'admin.block.update' => [
            Categories::class,
        ],
        'admin.type.create' => [
            Categories::class,
        ],
        'admin.type.index' => [
            Categories::class,
        ],
        'admin.type.update' => [
            Categories::class,
        ],
        'admin.vote._filter_form' => [
            Blocks::class,
        ],
    ];

    public function boot(): void
    {
        foreach ($this->viewComposers as $viewName => $classes) {
            $this->bootViewComposers($viewName, $classes);
        }
    }

    protected function bootViewComposers(string $view, array $classes): void
    {
        foreach ($classes as $className) {
            View::composer($view, $className);
        }
    }
}
