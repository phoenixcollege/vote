<?php

namespace App\Providers;

use App\Services\Image\Creator;
use Illuminate\Http\Middleware\TrustProxies;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;
use Smorken\Image\Sizer\Contracts\Sizer;
use Smorken\SocialAuth\SocialiteProviders\AzureLatestExtendSocialite;
use SocialiteProviders\Manager\SocialiteWasCalled;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerSocialAuthEvents();
        $this->setTrustedProxies();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Contracts\Image\Creator::class, fn ($app) => new Creator($app[Sizer::class]));
    }

    protected function registerSocialAuthEvents(): void
    {
        Event::listen(SocialiteWasCalled::class, AzureLatestExtendSocialite::class);
        //Event::listen(SocialiteWasCalled::class, FakeExtendSocialite::class);
    }

    protected function setTrustedProxies(): void
    {
        TrustProxies::at('*');
    }
}
