<?php

namespace App\Providers\Services;

use App\Contracts\Services\Candidates\RetrieveService;
use App\Contracts\Storage\Candidate;
use Smorken\Service\Invokables\Invokable;

class CandidateServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(RetrieveService::class, fn ($app) => new \App\Services\Candidates\RetrieveService($app[Candidate::class]));
    }
}
