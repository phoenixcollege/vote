<?php

namespace App\Providers\Services;

use App\Contracts\Services\Blocks\RetrieveService;
use App\Contracts\Services\Votes\SaveService;
use App\Contracts\Storage\Vote;
use Smorken\Service\Invokables\Invokable;

class VoteServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(\App\Contracts\Services\Votes\VoteServices::class, function ($app) {
            $services = [
                RetrieveService::class => $app[RetrieveService::class],
                \App\Contracts\Services\Candidates\RetrieveService::class => $app[\App\Contracts\Services\Candidates\RetrieveService::class],
                SaveService::class => new \App\Services\Votes\SaveService(
                    $app[Vote::class],
                    [
                        RetrieveService::class => $app[RetrieveService::class],
                    ]
                ),
            ];

            return new \App\Services\Votes\VoteServices($services);
        });
    }
}
