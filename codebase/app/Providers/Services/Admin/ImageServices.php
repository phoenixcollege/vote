<?php

namespace App\Providers\Services\Admin;

use App\Contracts\Image\Creator;
use App\Contracts\Services\Images\Admin\DeleteService;
use App\Contracts\Services\Images\Admin\SaveService;
use App\Contracts\Storage\Image;
use App\Services\Candidates\Admin\FilterService;
use Smorken\Service\Invokables\Invokable;

class ImageServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(\App\Contracts\Services\Images\Admin\ImageServices::class, function ($app) {
            $provider = $app[Image::class];
            $filterService = new FilterService();
            $services = [
                DeleteService::class => new \App\Services\Images\Admin\DeleteService($provider),
                SaveService::class => new \App\Services\Images\Admin\SaveService(
                    $provider,
                    $app[Creator::class]
                ),
                \Smorken\Service\Contracts\Services\FilterService::class => $filterService,
            ];

            return new \App\Services\Images\Admin\ImageServices($services);
        });
    }
}
