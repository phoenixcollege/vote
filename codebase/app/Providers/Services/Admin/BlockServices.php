<?php

namespace App\Providers\Services\Admin;

use App\Contracts\Services\Blocks\Admin\CrudServices;
use App\Contracts\Services\Blocks\Admin\IndexService;
use App\Contracts\Storage\Block;
use Illuminate\Contracts\Validation\Factory;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\Service\Invokables\Invokable;

class BlockServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(IndexService::class, fn ($app) => new \App\Services\Blocks\Admin\IndexService(
            $app[Block::class],
            [
                FilterService::class => new \App\Services\Blocks\Admin\FilterService(),
            ]
        ));
        $this->getApp()->bind(CrudServices::class, function ($app) {
            $provider = $app[Block::class];
            $services = [
                FilterService::class => new \App\Services\Blocks\Admin\FilterService(),
                ValidatorService::class => new \Smorken\Service\Services\ValidatorService($app[Factory::class]),
            ];

            return \App\Services\Blocks\Admin\CrudServices::createByStorageProvider($provider, $services);
        });
    }
}
