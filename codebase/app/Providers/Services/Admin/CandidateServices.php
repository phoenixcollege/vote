<?php

namespace App\Providers\Services\Admin;

use App\Contracts\Services\Candidates\Admin\CrudServices;
use App\Contracts\Services\Candidates\Admin\IndexService;
use App\Contracts\Storage\Candidate;
use Illuminate\Contracts\Validation\Factory;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\Service\Invokables\Invokable;

class CandidateServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(IndexService::class, fn ($app) => new \App\Services\Candidates\Admin\IndexService(
            $app[Candidate::class],
            [
                FilterService::class => new \App\Services\Candidates\Admin\FilterService(),
            ]
        ));
        $this->getApp()->bind(CrudServices::class, function ($app) {
            $provider = $app[Candidate::class];
            $services = [
                FilterService::class => new \App\Services\Candidates\Admin\FilterService(),
                ValidatorService::class => new \Smorken\Service\Services\ValidatorService($app[Factory::class]),
            ];

            return \App\Services\Candidates\Admin\CrudServices::createByStorageProvider($provider, $services);
        });
    }
}
