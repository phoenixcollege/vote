<?php

namespace App\Providers\Services\Admin;

use App\Contracts\Services\Types\Admin\CrudServices;
use App\Contracts\Services\Types\Admin\IndexService;
use App\Contracts\Storage\Type;
use Illuminate\Contracts\Validation\Factory;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\Service\Invokables\Invokable;

class TypeServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(IndexService::class, fn ($app) => new \App\Services\Types\Admin\IndexService(
            $app[Type::class],
            [
                FilterService::class => new \App\Services\Types\Admin\FilterService(),
            ]
        ));
        $this->getApp()->bind(CrudServices::class, function ($app) {
            $provider = $app[Type::class];
            $services = [
                FilterService::class => new \App\Services\Types\Admin\FilterService(),
                ValidatorService::class => new \Smorken\Service\Services\ValidatorService($app[Factory::class]),
            ];

            return \App\Services\Types\Admin\CrudServices::createByStorageProvider($provider, $services);
        });
    }
}
