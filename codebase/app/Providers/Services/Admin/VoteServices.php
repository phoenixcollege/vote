<?php

namespace App\Providers\Services\Admin;

use App\Contracts\Services\Votes\Admin\CrudServices;
use App\Contracts\Services\Votes\Admin\ExportService;
use App\Contracts\Services\Votes\Admin\IndexService;
use App\Contracts\Storage\Block;
use App\Contracts\Storage\Vote;
use Illuminate\Contracts\Validation\Factory;
use Smorken\Export\Contracts\Export;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\Service\Invokables\Invokable;

class VoteServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(IndexService::class, fn ($app) => new \App\Services\Votes\Admin\IndexService(
            $app[Vote::class],
            $app[Block::class],
            [
                FilterService::class => new \App\Services\Votes\Admin\FilterService(),
            ]
        ));
        $this->getApp()->bind(ExportService::class, fn ($app) => new \App\Services\Votes\Admin\ExportService(
            $app[Export::class],
            $app[Block::class],
            [
                FilterService::class => new \App\Services\Votes\Admin\FilterService(),
            ]
        ));
        $this->getApp()->bind(CrudServices::class, function ($app) {
            $provider = $app[Vote::class];
            $services = [
                FilterService::class => new \App\Services\Votes\Admin\FilterService(),
                ValidatorService::class => new \Smorken\Service\Services\ValidatorService($app[Factory::class]),
            ];

            return \App\Services\Votes\Admin\CrudServices::createByStorageProvider($provider, $services);
        });
    }
}
