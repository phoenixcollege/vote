<?php

namespace App\Providers\Services;

use App\Contracts\Services\Blocks\IndexService;
use App\Contracts\Services\Blocks\RetrieveService;
use App\Contracts\Storage\Block;
use App\Services\Charts\Google;
use Smorken\Service\Invokables\Invokable;

class BlockServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(IndexService::class, fn ($app) => new \App\Services\Blocks\IndexService(
            $app[Block::class],
            new Google()
        ));
        $this->getApp()->bind(RetrieveService::class, fn ($app) => new \App\Services\Blocks\RetrieveService(
            $app[Block::class]
        ));
    }
}
