<?php

namespace App\Providers\Services;

use App\Contracts\Services\Images\RetrieveService;
use App\Contracts\Storage\Image;
use Smorken\Service\Invokables\Invokable;

class ImageServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(RetrieveService::class, fn ($app) => new \App\Services\Images\RetrieveService($app[Image::class]));
    }
}
