<?php

declare(strict_types=1);

namespace App\Validation\RulesProviders;

class CategoryRules
{
    public static function rules(array $overrides = []): array
    {
        return [
            'name' => 'required|string|max:64',
            ...$overrides,
        ];
    }
}
