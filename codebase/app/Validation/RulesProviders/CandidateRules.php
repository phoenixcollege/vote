<?php

declare(strict_types=1);

namespace App\Validation\RulesProviders;

class CandidateRules
{
    public static function rules(array $overrides = []): array
    {
        return [
            'name' => 'required|string|max:64',
            'block_id' => 'required|integer',
            'type_id' => 'required|integer',
            'title' => 'required|string|max:64',
            'bio' => 'required',
            ...$overrides,
        ];
    }
}
