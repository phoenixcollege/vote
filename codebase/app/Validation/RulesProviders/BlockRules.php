<?php

declare(strict_types=1);

namespace App\Validation\RulesProviders;

class BlockRules
{
    public static function rules(array $overrides = []): array
    {
        return [
            'name' => 'required|string|max:64',
            'start' => 'date|required|before:end',
            'end' => 'date|required|after:start',
            'category_id' => 'required|integer',
            'private' => 'boolean',
            ...$overrides,
        ];
    }
}
