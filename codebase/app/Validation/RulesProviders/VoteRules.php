<?php

declare(strict_types=1);

namespace App\Validation\RulesProviders;

class VoteRules
{
    public static function rules(array $overrides = []): array
    {
        return [
            'user_id' => 'required|integer',
            'candidate_id' => 'required|integer',
            ...$overrides,
        ];
    }
}
