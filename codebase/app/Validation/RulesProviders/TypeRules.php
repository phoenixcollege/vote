<?php

declare(strict_types=1);

namespace App\Validation\RulesProviders;

class TypeRules
{
    public static function rules(array $overrides = []): array
    {
        return [
            'name' => 'required|string|max:64',
            'category_id' => 'required|integer',
            ...$overrides,
        ];
    }
}
