<?php

declare(strict_types=1);

namespace App\Validation\RulesProviders;

class ImageRules
{
    public static function rules(array $overrides = []): array
    {
        return [
            'mime' => 'required|max:32',
            'size' => 'required|int',
            'data' => 'required',
            'candidate_id' => 'required|int',
            'default' => 'boolean',
            ...$overrides,
        ];
    }
}
