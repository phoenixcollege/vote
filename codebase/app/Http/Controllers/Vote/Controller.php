<?php

namespace App\Http\Controllers\Vote;

use App\Contracts\Services\Blocks\RetrieveService;
use App\Contracts\Services\Votes\SaveService;
use App\Contracts\Services\Votes\VoteServices;
use App\Http\Controllers\HomeController;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Smorken\Service\Services\VO\RedirectActionResult;

class Controller extends \Smorken\Controller\View\Controller
{
    protected string $baseView = 'vote';

    public function __construct(
        protected VoteServices $factory
    ) {
        parent::__construct();
    }

    public function doSave(Request $request, int $id): RedirectResponse
    {
        $result = $this->getSaveService()->save($request, $id);
        if ($result->saved) {
            $request->session()
                ->flash('flash:success', 'Votes saved.');
        } else {
            $request->session()
                ->flash('flash:danger', 'Unable to save votes.');
        }
        foreach ($result->flashMessages as $key => $message) {
            $request->session()->flash($key, $message);
        }

        return (new RedirectActionResult([HomeController::class, 'index']))->redirect();
    }

    public function index(Request $request, int $id): \Illuminate\Contracts\View\View
    {
        $result = $this->getBlockRetrieveService()->findByIdWithUser($request, $id);

        return $this->makeView($this->getViewName('index'))
            ->with('block', $result->model);
    }

    public function viewCandidate(Request $request, int $id, int $c_id): \Illuminate\Contracts\View\View
    {
        $result = $this->getCandidateRetrieveService()->findById($c_id);

        return $this->makeView($this->getViewName('candidate'))
            ->with('block_id', $id)
            ->with('candidate', $result->model);
    }

    protected function getBlockRetrieveService(): RetrieveService
    {
        return $this->factory->getService(RetrieveService::class);
    }

    protected function getCandidateRetrieveService(): \App\Contracts\Services\Candidates\RetrieveService
    {
        return $this->factory->getService(\App\Contracts\Services\Candidates\RetrieveService::class);
    }

    protected function getSaveService(): SaveService
    {
        return $this->factory->getService(SaveService::class);
    }
}
