<?php

namespace App\Http\Controllers\Admin\Candidate;

use App\Contracts\Services\Candidates\Admin\CrudServices;
use App\Contracts\Services\Candidates\Admin\IndexService;
use App\Contracts\Services\Images\Admin\ImageServices;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Smorken\Controller\View\WithService\CrudController;
use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\Service\Services\VO\RedirectActionResult;

class Controller extends CrudController
{
    protected string $baseView = 'admin.candidate';

    public function __construct(
        protected ImageServices $imageServices,
        CrudServices $crudServices,
        IndexService $indexService
    ) {
        parent::__construct($crudServices, $indexService);
    }

    public function defaultImage(Request $request, int $id, int $image_id): RedirectResponse
    {
        if ($this->imageServices->getSaveService()->setDefault($request, $image_id)) {
            $request->session()
                ->flash('flash:success', "Updated default image [$image_id].");
        } else {
            $request->session()
                ->flash('flash:danger', "Unable to update default image [$image_id].");
        }
        $params = array_merge($this->imageServices->getCandidateFilterService()
            ->getFilterFromRequest($request)
            ->toArray(), ['id' => $id]);

        return (new RedirectActionResult($this->actionArray('view'), $params))->redirect();
    }

    public function deleteImage(Request $request, int $id, int $image_id): RedirectResponse
    {
        $result = $this->imageServices->getDeleteService()->deleteFromRequest($request, $image_id);
        $this->addMessagesToRequest($request, $result);
        if ($result->result) {
            $request->session()
                ->flash('flash:success', "Deleted image [$image_id].");
        } else {
            $request->session()
                ->flash('flash:danger', "Could not delete image [$image_id].");
        }
        $params = array_merge($this->imageServices->getCandidateFilterService()
            ->getFilterFromRequest($request)
            ->toArray(), ['id' => $id]);

        return (new RedirectActionResult($this->actionArray('view'), $params))->redirect();
    }

    protected function onSaveSuccess(Request $request, ModelResult $result): void
    {
        $result = $this->imageServices->getSaveService()->save($request, $result->model);
        foreach ($result->flashMessages as $key => $message) {
            $request->session()->flash($key, $message);
        }
    }
}
