<?php

namespace App\Http\Controllers\Admin\Block;

use App\Contracts\Services\Blocks\Admin\CrudServices;
use App\Contracts\Services\Blocks\Admin\IndexService;
use Smorken\Controller\View\WithService\CrudController;

class Controller extends CrudController
{
    protected string $baseView = 'admin.block';

    public function __construct(CrudServices $crudServices, IndexService $indexService)
    {
        parent::__construct($crudServices, $indexService);
    }
}
