<?php

namespace App\Http\Controllers\Admin\Vote;

use App\Contracts\Services\Votes\Admin\CrudServices;
use App\Contracts\Services\Votes\Admin\ExportService;
use App\Contracts\Services\Votes\Admin\IndexService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Smorken\Controller\View\WithService\Concerns\HasDelete;

class Controller extends \Smorken\Controller\View\Controller
{
    use HasDelete;

    protected string $baseView = 'admin.vote';

    public function __construct(
        protected ExportService $exportService,
        protected IndexService $indexService,
        CrudServices $crudServices
    ) {
        $this->crudServices = $crudServices;
        parent::__construct();
    }

    public function export(Request $request): Response
    {
        $result = $this->exportService->export($request);

        return $result->exporter->output('votes');
    }

    public function index(Request $request): \Illuminate\Contracts\View\View
    {
        $result = $this->indexService->getByRequest($request);

        return $this->makeView($this->getIndexView($result->filter))
            ->with('models', $result->models)
            ->with('filter', $result->filter);
    }

    protected function getIndexView(\Smorken\Support\Contracts\Filter $filter): string
    {
        if ($filter->aggregate) {
            return $this->getViewName('index_aggregate');
        }

        return $this->getViewName('index');
    }
}
