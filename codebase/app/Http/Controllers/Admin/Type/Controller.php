<?php

namespace App\Http\Controllers\Admin\Type;

use App\Contracts\Services\Types\Admin\CrudServices;
use App\Contracts\Services\Types\Admin\IndexService;
use Smorken\Controller\View\WithService\CrudController;

class Controller extends CrudController
{
    protected string $baseView = 'admin.type';

    public function __construct(CrudServices $crudServices, IndexService $indexService)
    {
        parent::__construct($crudServices, $indexService);
    }
}
