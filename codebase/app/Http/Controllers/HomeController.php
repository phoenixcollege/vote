<?php

namespace App\Http\Controllers;

use App\Contracts\Services\Blocks\IndexService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class HomeController extends \Smorken\Controller\View\Controller
{
    public function __construct(protected IndexService $indexService)
    {
        parent::__construct();
    }

    public function doIndex(Request $request): RedirectResponse
    {
        $result = $this->indexService->getRedirector($request);

        return $result->redirect();
    }

    public function index(Request $request): \Illuminate\Contracts\View\View
    {
        $result = $this->indexService->getByRequest($request);

        return $this->makeView($this->getViewName('home'))
            ->with('charts', $result->charts)
            ->with('blocks', $result->blocks);
    }
}
