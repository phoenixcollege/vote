<?php

namespace App\Http\Controllers\Image;

use App\Contracts\Services\Images\RetrieveService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class Controller extends \Smorken\Controller\View\Controller
{
    public function __construct(protected RetrieveService $retrieveService)
    {
        parent::__construct();
    }

    public function view(Request $request, int $id): Response
    {
        $result = $this->retrieveService->findById($id);

        return \Illuminate\Support\Facades\Response::make($result->model->data, 200,
            ['Content-Type' => $result->model->mime]);
    }
}
