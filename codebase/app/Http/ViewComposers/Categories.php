<?php

namespace App\Http\ViewComposers;

use App\Contracts\Storage\Category;
use Illuminate\Contracts\View\View;

class Categories
{
    public function __construct(protected Category $category)
    {
    }

    public function compose(View $view): void
    {
        $categories = $this->category->all();
        $view->with('categories', $categories);
    }
}
