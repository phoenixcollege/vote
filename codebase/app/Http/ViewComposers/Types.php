<?php

namespace App\Http\ViewComposers;

use App\Contracts\Storage\Type;
use Illuminate\Contracts\View\View;

class Types
{
    public function __construct(protected Type $type)
    {
    }

    public function compose(View $view): void
    {
        $types = $this->type->all();
        $view->with('types', $types);
    }
}
