<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;

class Sanitize
{
    public function __construct(protected \Smorken\Sanitizer\Contracts\Sanitize $sanitize)
    {
    }

    public function compose(View $view): void
    {
        $view->with('sanitize', $this->sanitize);
    }
}
