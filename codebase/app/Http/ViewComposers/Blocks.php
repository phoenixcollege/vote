<?php

namespace App\Http\ViewComposers;

use App\Contracts\Storage\Block;
use Illuminate\Contracts\View\View;

class Blocks
{
    public function __construct(protected Block $block)
    {
    }

    public function compose(View $view): void
    {
        $blocks = $this->block->all();
        $view->with('blocks', $blocks);
    }
}
