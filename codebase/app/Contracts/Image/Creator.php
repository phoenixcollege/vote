<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/31/16
 * Time: 11:45 AM
 */

namespace App\Contracts\Image;

use Smorken\Image\Sizer\Contracts\Result;

interface Creator
{
    /**
     * Will return an image's binary string
     * or null
     */
    public function create(string $raw): Result;
}
