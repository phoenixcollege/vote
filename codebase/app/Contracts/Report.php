<?php

namespace App\Contracts;

use Illuminate\Support\Collection;

interface Report
{
    public function get(Collection $items): Collection;
}
