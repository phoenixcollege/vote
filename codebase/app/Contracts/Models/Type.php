<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/12/15
 * Time: 10:37 AM
 */

namespace App\Contracts\Models;

/**
 * Interface Type
 *
 * @property int $id
 * @property string $name
 * @property int $category_id
 * @property Category $category
 */
interface Type
{
}
