<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/12/15
 * Time: 10:38 AM
 */

namespace App\Contracts\Models;

/**
 * Interface Candidate
 *
 * @property int $id
 * @property string $name
 * @property int $type_id
 * @property int $block_id
 * @property string $title
 * @property string $bio
 * @property Type $type
 * @property Block Block
 * @property Vote[] $votes
 * @property Image[] $images
 * @property Image $image
 */
interface Candidate
{
}
