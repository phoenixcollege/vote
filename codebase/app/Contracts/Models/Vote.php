<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/12/15
 * Time: 10:39 AM
 */

namespace App\Contracts\Models;

/**
 * Interface Vote
 *
 * @property int $id
 * @property string $user_id
 * @property int $candidate_id
 * @property Candidate $candidate
 */
interface Vote
{
}
