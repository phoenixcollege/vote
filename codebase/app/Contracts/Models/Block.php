<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/12/15
 * Time: 10:36 AM
 */

namespace App\Contracts\Models;

use Illuminate\Support\Collection;

/**
 * Interface Block
 *
 *
 * @property int $id
 * @property string $name
 * @property \DateTime $start
 * @property \DateTime $end
 * @property int $category_id
 * @property Category $category
 * @property Candidate[] $candidates
 */
interface Block
{
    public function currentActive(): ?Block;

    public function getCountForCandidates(): Collection;

    public function isOpen(): bool;

    public function lastActive(): ?Block;

    public function toDateTimeLocalFormat(string $attribute): ?string;
}
