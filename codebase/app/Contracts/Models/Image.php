<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/12/15
 * Time: 10:37 AM
 */

namespace App\Contracts\Models;

/**
 * Interface Image
 *
 * @property string $id
 * @property string $name
 * @property string $filename
 * @property string $size
 * @property string $mime
 * @property string $data
 * @property int $candidate_id
 * @property Candidate[] $candidate
 */
interface Image
{
}
