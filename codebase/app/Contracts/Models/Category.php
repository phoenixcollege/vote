<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/13/15
 * Time: 10:21 AM
 */

namespace App\Contracts\Models;

/**
 * Interface Category
 *
 * @property int $id
 * @property string $name
 * @property Block[] $blocks
 * @property Type[] $types
 */
interface Category
{
}
