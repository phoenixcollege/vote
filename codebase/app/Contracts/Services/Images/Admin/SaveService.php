<?php

namespace App\Contracts\Services\Images\Admin;

use App\Contracts\Image\Creator;
use App\Contracts\Models\Candidate;
use App\Contracts\Storage\Image;
use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\BaseService;

interface SaveService extends BaseService
{
    public function getCreator(): Creator;

    public function getProvider(): Image;

    public function save(Request $request, Candidate $candidate): SaveResult;

    public function setDefault(Request $request, int $id): bool;
}
