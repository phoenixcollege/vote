<?php

namespace App\Contracts\Services\Images\Admin;

use Smorken\Service\Contracts\Services\VO\VOResult;

/**
 * @property \App\Contracts\Models\Candidate $candidate
 * @property bool $hasImage
 * @property bool $saved
 * @property array $flashMessages = []
 */
interface SaveResult extends VOResult
{
}
