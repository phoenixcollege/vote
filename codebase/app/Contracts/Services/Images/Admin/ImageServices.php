<?php

namespace App\Contracts\Services\Images\Admin;

use Smorken\Service\Contracts\Services\Factory;
use Smorken\Service\Contracts\Services\FilterService;

interface ImageServices extends Factory
{
    public function getCandidateFilterService(): FilterService;

    public function getDeleteService(): DeleteService;

    public function getSaveService(): SaveService;
}
