<?php

namespace App\Contracts\Services\Votes;

use Smorken\Service\Contracts\Services\VO\VOResult;

/**
 * @property bool $saved
 * @property array $flashMessages
 */
interface SaveResult extends VOResult
{
}
