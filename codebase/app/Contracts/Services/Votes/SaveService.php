<?php

namespace App\Contracts\Services\Votes;

use App\Contracts\Services\Blocks\RetrieveService;
use App\Contracts\Storage\Vote;
use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\BaseService;

interface SaveService extends BaseService
{
    public function getBlockRetrieveService(): RetrieveService;

    public function getVoteProvider(): Vote;

    public function save(Request $request, int $id): SaveResult;
}
