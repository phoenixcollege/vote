<?php

namespace App\Contracts\Services\Votes\Admin;

use App\Contracts\Storage\Block;
use App\Contracts\Storage\Vote;
use Smorken\Service\Contracts\Services\HasFilterService;

interface IndexService extends \Smorken\Service\Contracts\Services\IndexService, HasFilterService
{
    public function getBlockProvider(): Block;

    public function getProvider(): Vote;
}
