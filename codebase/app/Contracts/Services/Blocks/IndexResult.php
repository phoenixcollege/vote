<?php

namespace App\Contracts\Services\Blocks;

use Smorken\Service\Contracts\Services\VO\VOResult;

/**
 * @property \Illuminate\Support\Collection<\App\Contracts\Models\Block> $blocks
 * @property \Illuminate\Support\Collection $charts
 */
interface IndexResult extends VOResult
{
}
