<?php

namespace App\Contracts\Services\Blocks;

use App\Contracts\Storage\Block;
use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\VO\ModelResult;

interface RetrieveService extends \Smorken\Service\Contracts\Services\RetrieveService
{
    public function findByIdWithUser(Request $request, int $id): ModelResult;

    public function getProvider(): Block;
}
