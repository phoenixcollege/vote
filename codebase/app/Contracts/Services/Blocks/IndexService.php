<?php

namespace App\Contracts\Services\Blocks;

use App\Contracts\Report;
use App\Contracts\Storage\Block;
use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\BaseService;
use Smorken\Service\Contracts\Services\VO\RedirectActionResult;

interface IndexService extends BaseService
{
    public function getByRequest(Request $request): IndexResult;

    public function getProvider(): Block;

    public function getRedirector(Request $request): RedirectActionResult;

    public function getReporter(): Report;
}
