<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/12/15
 * Time: 10:39 AM
 */

namespace App\Contracts\Storage;

use Smorken\Storage\Contracts\Base;

/**
 * Interface Vote
 */
interface Vote extends Base
{
    public function saveVotes(int $block_id, string $user_id, array $type_candidate_arr): bool;
}
