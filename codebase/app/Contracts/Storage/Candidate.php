<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/12/15
 * Time: 10:38 AM
 */

namespace App\Contracts\Storage;

use Smorken\Storage\Contracts\Base;

/**
 * Interface Candidate
 */
interface Candidate extends Base
{
}
