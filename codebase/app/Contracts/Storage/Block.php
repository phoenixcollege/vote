<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/12/15
 * Time: 10:36 AM
 */

namespace App\Contracts\Storage;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Storage\Contracts\Base;
use Smorken\Support\Contracts\Filter;

/**
 * Interface Block
 */
interface Block extends Base
{
    public function activeAndUpcomingSelect(): Collection;

    public function chunkWithVotes(Filter $filter, callable $callback, int $perChunk = 100): void;

    public function currentBlockSelect(): Collection;

    public function currentBlocks(bool $all = true): Collection;

    public function currentWithCounts(bool $all = true): Collection;

    public function getBlockWithVotesForUser(int $block_id, string $user_id): \App\Contracts\Models\Block;

    public function getByFilterWithCounts(Filter $filter, int $per_page = 20): \Traversable;

    public function getWithVotes(Filter $filter): \Traversable;

    public function lastByCount(int $count = 20): Collection|Paginator;
}
