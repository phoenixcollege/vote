<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/12/15
 * Time: 12:59 PM
 */

namespace App\Contracts\Storage;

use Smorken\Image\Sizer\Contracts\Result;
use Smorken\Storage\Contracts\Base;

interface Image extends Base
{
    public function addNewImage(Result $imagedata, int $candidate_id): \App\Contracts\Models\Image;

    public function setImageDefault(int $id): bool;
}
