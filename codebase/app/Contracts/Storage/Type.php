<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/12/15
 * Time: 10:37 AM
 */

namespace App\Contracts\Storage;

use Illuminate\Support\Collection;
use Smorken\Storage\Contracts\Base;

/**
 * Interface Type
 */
interface Type extends Base
{
    public function byCategorySelect(int $category_id): Collection;
}
