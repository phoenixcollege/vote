<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/12/15
 * Time: 10:40 AM
 */

namespace App\Storage\Eloquent;

use App\Validation\RulesProviders\CandidateRules;
use Illuminate\Contracts\Database\Query\Builder;
use Smorken\Support\Contracts\Filter;

class Candidate extends Base implements \App\Contracts\Storage\Candidate
{
    public function getByFilter(Filter $filter, int $perPage = 20): \Traversable
    {
        $q = $this->getModel()
            ->newQuery()
            ->orderDefault()
            ->with('block')
            ->with('type');
        $allowed = [
            'block_id' => 'filterBlockId',
            'type_id' => 'filterTypeId',
        ];
        foreach ($filter->toArray() as $k => $v) {
            if (isset($allowed[$k])) {
                $method = $allowed[$k];
                $q = $this->$method($q, $v);
            }
        }

        return $this->limitOrPaginate($q, $perPage);
    }

    public function validationRules(array $override = []): array
    {
        return CandidateRules::rules($override);
    }

    protected function filterBlockId(Builder $query, $value): Builder
    {
        if (strlen((string) $value)) {
            $query = $query->blockIdIs($value);
        }

        return $query;
    }

    protected function filterTypeId(Builder $query, $value): Builder
    {
        if (strlen((string) $value)) {
            $query = $query->typeIdIs($value);
        }

        return $query;
    }
}
