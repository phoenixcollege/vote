<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/12/15
 * Time: 10:40 AM
 */

namespace App\Storage\Eloquent;

use App\Validation\RulesProviders\TypeRules;
use Illuminate\Contracts\Database\Query\Builder;
use Illuminate\Support\Collection;
use Smorken\Support\Contracts\Filter;

class Type extends Base implements \App\Contracts\Storage\Type
{
    public function byCategorySelect(int $category_id): Collection
    {
        return $this->getCacheAssist()->remember(['select', $category_id],
            $this->getCacheAssist()->getCacheOptions()->defaultCacheTime, fn () => $this->getModel()
                ->where('category_id', $category_id)
                ->orderBy('name')
                ->get());
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getByFilter(Filter $filter, int $per_page = 20): \Traversable
    {
        $q = $this->getModel()
            ->newQuery()
            ->orderDefault()
            ->with('category');
        $allowed = [
            'category_id' => 'filterCategoryId',
        ];
        foreach ($filter->toArray() as $k => $v) {
            if (isset($allowed[$k])) {
                $method = $allowed[$k];
                $q = $this->$method($q, $v);
            }
        }

        return $this->limitOrPaginate($q, $per_page);
    }

    public function validationRules(array $override = []): array
    {
        return TypeRules::rules($override);
    }

    protected function filterCategoryId(Builder $query, $value): Builder
    {
        if (strlen((string) $value)) {
            $query = $query->categoryIdIs($value);
        }

        return $query;
    }
}
