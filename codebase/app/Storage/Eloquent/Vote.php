<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/12/15
 * Time: 10:40 AM
 */

namespace App\Storage\Eloquent;

use App\Validation\RulesProviders\VoteRules;
use Illuminate\Contracts\Database\Query\Builder;

class Vote extends Base implements \App\Contracts\Storage\Vote
{
    public function saveVotes(int $block_id, string $user_id, array $type_candidate_arr): bool
    {
        $saved = true;
        $this->getModel()
            ->whereHas('candidate', function ($q) use ($block_id) {
                $q->where('block_id', '=', $block_id);
            })
            ->where('user_id', $user_id)
            ->delete();
        foreach ($type_candidate_arr as $type_id => $candidate_id) {
            $data = [
                'candidate_id' => $candidate_id,
                'user_id' => $user_id,
            ];
            $m = $this->getModel()
                ->newInstance($data);
            if (! $m->save()) {
                $saved = false;
            }
        }

        return $saved;
    }

    public function validationRules(array $override = []): array
    {
        return VoteRules::rules($override);
    }

    protected function filterBlockId(Builder $query, $value): Builder
    {
        if (strlen((string) $value)) {
            $query = $query->hasBlockId($value);
        }

        return $query;
    }

    protected function getFilterMethods(): array
    {
        return ['block_id' => 'filterBlockId'];
    }
}
