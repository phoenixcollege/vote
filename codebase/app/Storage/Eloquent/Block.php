<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/12/15
 * Time: 10:40 AM
 */

namespace App\Storage\Eloquent;

use App\Validation\RulesProviders\BlockRules;
use Carbon\Carbon;
use Illuminate\Contracts\Database\Query\Builder;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Support\Contracts\Filter;

class Block extends Base implements \App\Contracts\Storage\Block
{
    public function activeAndUpcomingSelect(): Collection
    {
        return $this->getCacheAssist()->remember(['activeupcoming'], Carbon::now()->addHour(), fn () => $this->getModel()
            ->whereDate('end', '>=', date('Y-m-d'))
            ->orderBy('end', 'ASC')
            ->get());
    }

    public function chunkWithVotes(Filter $filter, callable $callback, int $perChunk = 100): void
    {
        $q = $this->getQueryFromFilterWithCounts($filter);
        $q->chunk($perChunk, $callback);
    }

    public function currentBlockSelect(): Collection
    {
        return $this->getCacheAssist()->remember(['current'], Carbon::now()->addHour(), fn () => $this->currentBaseQuery()
            ->get());
    }

    public function currentBlocks(bool $all = true): Collection
    {
        return $this->currentBaseQuery($all)
            ->get();
    }

    public function currentWithCounts(bool $all = true): Collection
    {
        $q = $this->currentBaseQuery($all);

        return $q->with('candidates')
            ->with('candidates.type')
            ->with('candidates.votes')
            ->get();
    }

    public function getBlockWithVotesForUser(int $block_id, string $user_id): \App\Contracts\Models\Block
    {
        return $this->getModel()
            ->with([
                'category',
                'category.types',
                'candidates.image',
                'candidates.votes' => function ($q) use ($user_id) {
                    $q->where('user_id', '=', $user_id);
                },
            ])
            ->where('id', '=', $block_id)
            ->firstOrFail();
    }

    public function getByFilterWithCounts(Filter $filter, int $per_page = 20): \Traversable
    {
        $q = $this->getQueryFromFilterWithCounts($filter);

        return $this->limitOrPaginate($q, $per_page);
    }

    public function getWithVotes(Filter $filter): \Traversable
    {
        $q = $this->queryFromFilter($filter);
        $q = $q->with([
            'candidates',
            'candidates.type',
            'candidates.votes',
        ]);

        return $q->get();
    }

    public function lastByCount(int $count = 20): Collection|Paginator
    {
        $q = $this->getModel()
            ->orderBy('end', 'asc');

        return $this->limitOrPaginate($q, $count);
    }

    public function validationRules(array $override = []): array
    {
        return BlockRules::rules($override);
    }

    protected function currentBaseQuery(bool $all = true): Builder
    {
        $now = date('Y-m-d H:i:s');
        $r = $this->getModel()
            ->newQuery()
            ->where('start', '<=', $now)
            ->where('end', '>=', $now)
            ->orderBy('end', 'ASC');
        if (! $all) {
            $r->where('private', 0);
        }

        return $r;
    }

    protected function filterCategoryIdIs(Builder $q, $value): Builder
    {
        if (strlen((string) $value)) {
            $q->categoryIdIs($value);
        }

        return $q;
    }

    protected function filterIdIs(Builder $q, $value): Builder
    {
        if (strlen((string) $value)) {
            return $q->idIs($value);
        }

        return $q;
    }

    protected function getFilterMethods(): array
    {
        return [
            'block_id' => 'filterIdIs',
            'category_id' => 'filterCategoryIdIs',
        ];
    }

    protected function getQueryFromFilterWithCounts(Filter $filter): Builder
    {
        $q = $this->queryFromFilter($filter);

        return $q->with([
            'candidates' => function ($sq) {
                $sq->with('type');
                $sq->withCount('votes');
            },
        ]);
    }

    protected function getReportData(): Collection
    {
        $q = $this->currentBaseQuery();
        $q->with('votes')
            ->with('votes.candidate')
            ->with('votes.candidate.type');

        return $q->get();
    }
}
