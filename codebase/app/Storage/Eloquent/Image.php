<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/28/16
 * Time: 7:48 AM
 */

namespace App\Storage\Eloquent;

use App\Validation\RulesProviders\ImageRules;
use Smorken\Image\Sizer\Contracts\Result;

class Image extends Base implements \App\Contracts\Storage\Image
{
    public function addNewImage(Result $imagedata, int $candidate_id): \App\Contracts\Models\Image
    {
        $data = [
            'size' => $imagedata->size,
            'data' => $imagedata->data,
            'mime' => $imagedata->mime,
            'candidate_id' => $candidate_id,
            'default' => 1,
        ];
        $this->getModel()->newQuery()->where('candidate_id', $candidate_id)->update(['default' => 0]);

        return $this->create($data);
    }

    public function setImageDefault(int $id): bool
    {
        $model = $this->getModel()->find($id);
        if ($model) {
            $this->getModel()->newQuery()->where('candidate_id', $model->candidate_id)->update(['default' => 0]);
            $model->default = true;

            return $model->save();
        }

        return false;
    }

    public function validationRules(array $override = []): array
    {
        return ImageRules::rules($override);
    }
}
