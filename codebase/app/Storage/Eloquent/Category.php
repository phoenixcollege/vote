<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/12/15
 * Time: 10:40 AM
 */

namespace App\Storage\Eloquent;

use App\Validation\RulesProviders\CategoryRules;

class Category extends Base implements \App\Contracts\Storage\Category
{
    public function validationRules(array $override = []): array
    {
        return CategoryRules::rules($override);
    }
}
