<?php

namespace App\Services\Images;

use Smorken\Service\Services\RetrieveByStorageProviderService;

class RetrieveService extends RetrieveByStorageProviderService implements \App\Contracts\Services\Images\RetrieveService
{
}
