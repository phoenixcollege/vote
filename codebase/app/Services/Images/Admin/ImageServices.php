<?php

namespace App\Services\Images\Admin;

use App\Contracts\Services\Images\Admin\DeleteService;
use App\Contracts\Services\Images\Admin\SaveService;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Services\Factory;

class ImageServices extends Factory implements \App\Contracts\Services\Images\Admin\ImageServices
{
    protected array $services = [
        FilterService::class => null,
        DeleteService::class => null,
        SaveService::class => null,
    ];

    public function getCandidateFilterService(): FilterService
    {
        return $this->getService(FilterService::class);
    }

    public function getDeleteService(): DeleteService
    {
        return $this->getService(DeleteService::class);
    }

    public function getSaveService(): SaveService
    {
        return $this->getService(SaveService::class);
    }
}
