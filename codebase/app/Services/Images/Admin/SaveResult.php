<?php

namespace App\Services\Images\Admin;

use App\Contracts\Models\Candidate;
use Smorken\Service\Services\VO\VOResult;

class SaveResult extends VOResult implements \App\Contracts\Services\Images\Admin\SaveResult
{
    public function __construct(
        public Candidate $candidate,
        public bool $hasImage,
        public bool $saved,
        public array $flashMessages = []
    ) {
    }
}
