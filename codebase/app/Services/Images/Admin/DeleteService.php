<?php

namespace App\Services\Images\Admin;

use Smorken\Service\Services\DeleteByStorageProviderService;

class DeleteService extends DeleteByStorageProviderService implements \App\Contracts\Services\Images\Admin\DeleteService
{
}
