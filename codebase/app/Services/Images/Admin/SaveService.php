<?php

namespace App\Services\Images\Admin;

use App\Contracts\Image\Creator;
use App\Contracts\Models\Candidate;
use App\Contracts\Services\Images\Admin\SaveResult;
use App\Contracts\Storage\Image;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Smorken\Service\Services\BaseService;

class SaveService extends BaseService implements \App\Contracts\Services\Images\Admin\SaveService
{
    protected string $voClass = \App\Services\Images\Admin\SaveResult::class;

    public function __construct(protected Image $imageProvider, protected Creator $creator, array $services = [])
    {
        parent::__construct($services);
    }

    public function getCreator(): Creator
    {
        return $this->creator;
    }

    public function getProvider(): Image
    {
        return $this->imageProvider;
    }

    public function save(Request $request, Candidate $candidate): SaveResult
    {
        $hasImage = $request->hasFile('photo');
        if (! $hasImage) {
            return $this->newVO([
                'candidate' => $candidate, 'hasImage' => false, 'saved' => false, 'flashMessages' => [],
            ]);
        }

        return $this->saveImage($request->file('photo'), $candidate);
    }

    public function setDefault(Request $request, int $id): bool
    {
        return $this->getProvider()->setImageDefault($id);
    }

    protected function saveImage(UploadedFile $file, Candidate $candidate): SaveResult
    {
        if (! $file->isValid()) {
            $messages = ['flash:danger' => $file->getErrorMessage()];

            return $this->newVO([
                'candidate' => $candidate, 'hasImage' => true, 'saved' => false, 'flashMessages' => $messages,
            ]);
        }
        $raw = file_get_contents($file->getPathname());
        if (! $raw) {
            $messages = ['flash:danger' => 'Unable to retrieve uploaded image.'];

            return $this->newVO([
                'candidate' => $candidate, 'hasImage' => true, 'saved' => false, 'flashMessages' => $messages,
            ]);
        }
        $imageData = $this->getCreator()->create($raw);
        $saved = (bool) $this->getProvider()->addNewImage($imageData, $candidate->id);

        return $this->newVO(['candidate' => $candidate, 'hasImage' => true, 'saved' => $saved]);
    }
}
