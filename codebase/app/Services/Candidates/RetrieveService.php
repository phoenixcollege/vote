<?php

namespace App\Services\Candidates;

use Smorken\Service\Services\RetrieveByStorageProviderService;

class RetrieveService extends RetrieveByStorageProviderService implements \App\Contracts\Services\Candidates\RetrieveService
{
}
