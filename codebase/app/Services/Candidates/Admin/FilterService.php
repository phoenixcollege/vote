<?php

namespace App\Services\Candidates\Admin;

use Illuminate\Http\Request;
use Smorken\Support\Contracts\Filter;

class FilterService extends \Smorken\Service\Services\FilterService
{
    protected function createFilterFromRequest(Request $request): Filter
    {
        return new \Smorken\Support\Filter([
            'block_id' => $request->input('block_id'),
            'type_id' => $request->input('type_id'),
        ]);
    }
}
