<?php

namespace App\Services\Candidates\Admin;

use Smorken\Service\Services\PaginateByFilterService;

class IndexService extends PaginateByFilterService implements \App\Contracts\Services\Candidates\Admin\IndexService
{
}
