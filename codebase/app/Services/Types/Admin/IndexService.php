<?php

namespace App\Services\Types\Admin;

use Smorken\Service\Services\PaginateByFilterService;

class IndexService extends PaginateByFilterService implements \App\Contracts\Services\Types\Admin\IndexService
{
}
