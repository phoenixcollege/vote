<?php

namespace App\Services\Charts;

use App\Contracts\Report;
use Illuminate\Support\Collection;

class Google implements Report
{
    public function get(Collection $items): Collection
    {
        $charts = new Collection();
        foreach ($items as $block) {
            $current = [
                'block' => $block,
                'charts' => [],
            ];
            foreach ($this->parseAggregateToChartable($block->candidates) as $type_id => $typeinfo) {
                $current['charts'][] = $typeinfo;
            }
            $charts->put($block->id, $current);
        }

        return $charts;
    }

    protected function parseAggregateToChartable(Collection $candidates): array
    {
        $chartable = [];
        foreach ($candidates as $candidate) {
            if (! isset($chartable[$candidate->type_id])) {
                $chartable[$candidate->type_id] = [
                    'name' => e($candidate->type ? $candidate->type->name : $candidate->type_id),
                    'datasets' => [],
                ];
            }
            $chartable[$candidate->type_id]['datasets'][e($candidate->name)] = $candidate->voteCount();
        }

        return $chartable;
    }
}
