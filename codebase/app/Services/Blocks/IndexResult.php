<?php

namespace App\Services\Blocks;

use Illuminate\Support\Collection;
use Smorken\Service\Services\VO\VOResult;

class IndexResult extends VOResult implements \App\Contracts\Services\Blocks\IndexResult
{
    public function __construct(
        public Collection $blocks,
        public Collection $charts
    ) {
    }
}
