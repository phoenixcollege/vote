<?php

namespace App\Services\Blocks;

use App\Contracts\Storage\Block;
use Illuminate\Http\Request;
use JetBrains\PhpStorm\ExpectedValues;
use Smorken\Service\Contracts\Enums\RetrieveTypes;
use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\Service\Contracts\Services\VO\VOResult;
use Smorken\Service\Services\BaseService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RetrieveService extends BaseService implements \App\Contracts\Services\Blocks\RetrieveService
{
    protected string $voClass = \Smorken\Service\Services\VO\ModelResult::class;

    public function __construct(protected Block $provider, $services = [])
    {
        parent::__construct($services);
    }

    public function findById(
        int|string $id,
        bool $shouldFail = true,
        #[ExpectedValues(valuesFromClass: RetrieveTypes::class)] string $type = RetrieveTypes::VIEW
    ): VOResult|ModelResult {
        $model = $this->getModelById($id, $shouldFail);

        return $this->newVO(['model' => $model, 'id' => $id, 'result' => (bool) $model]);
    }

    public function findByIdWithUser(Request $request, int $id): ModelResult
    {
        $model = $this->getProvider()->getBlockWithVotesForUser($id, $request->user()->id);
        if (! $model) {
            throw new NotFoundHttpException("Block [$id] was not found.");
        }

        return $this->newVO(['model' => $model, 'id' => $id, 'result' => (bool) $model]);
    }

    public function getProvider(): Block
    {
        return $this->provider;
    }

    protected function getModelById(int $id, bool $shouldFail): \App\Contracts\Models\Block
    {
        if ($shouldFail) {
            return $this->getProvider()->findOrFail($id);
        }

        return $this->getProvider()->find($id);
    }
}
