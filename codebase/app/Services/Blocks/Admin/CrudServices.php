<?php

namespace App\Services\Blocks\Admin;

use Smorken\Service\Services\CrudByStorageProviderServices;

class CrudServices extends CrudByStorageProviderServices implements \App\Contracts\Services\Blocks\Admin\CrudServices
{
}
