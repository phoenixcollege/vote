<?php

namespace App\Services\Blocks\Admin;

use Smorken\Service\Services\PaginateByFilterService;

class IndexService extends PaginateByFilterService implements \App\Contracts\Services\Blocks\Admin\IndexService
{
}
