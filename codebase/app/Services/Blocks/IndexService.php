<?php

namespace App\Services\Blocks;

use App\Contracts\Report;
use App\Contracts\Services\Blocks\IndexResult;
use App\Contracts\Storage\Block;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Vote\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Smorken\Service\Contracts\Services\VO\RedirectActionResult;
use Smorken\Service\Services\BaseService;

class IndexService extends BaseService implements \App\Contracts\Services\Blocks\IndexService
{
    protected string $voClass = \App\Services\Blocks\IndexResult::class;

    public function __construct(
        protected Block $blockProvider,
        protected Report $reportProvider,
        array $services = []
    ) {
        parent::__construct($services);
    }

    public function getByRequest(Request $request): IndexResult
    {
        $blocks = $this->getProvider()->currentBlockSelect();
        $charts = $this->getReporter()->get($this->getProvider()->currentWithCounts(false));

        return $this->newVO(['blocks' => $blocks, 'charts' => $charts]);
    }

    public function getProvider(): Block
    {
        return $this->blockProvider;
    }

    public function getRedirector(Request $request): RedirectActionResult
    {
        $id = $request->input('id');
        if ($id) {
            return new \Smorken\Service\Services\VO\RedirectActionResult([Controller::class, 'index'], ['id' => $id]);
        }

        return new \Smorken\Service\Services\VO\RedirectActionResult([HomeController::class, 'index'], [],
            new MessageBag(['block' => 'Select a voting block to continue.']));
    }

    public function getReporter(): Report
    {
        return $this->reportProvider;
    }
}
