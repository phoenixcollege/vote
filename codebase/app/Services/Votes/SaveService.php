<?php

namespace App\Services\Votes;

use App\Contracts\Models\Block;
use App\Contracts\Services\Blocks\RetrieveService;
use App\Contracts\Services\Votes\SaveResult;
use App\Contracts\Storage\Vote;
use Illuminate\Http\Request;
use Smorken\Service\Services\BaseService;

class SaveService extends BaseService implements \App\Contracts\Services\Votes\SaveService
{
    protected string $voClass = \App\Services\Votes\SaveResult::class;

    public function __construct(
        protected Vote $voteProvider,
        array $services = []
    ) {
        parent::__construct($services);
    }

    public function getBlockRetrieveService(): RetrieveService
    {
        return $this->getService(RetrieveService::class);
    }

    public function getVoteProvider(): Vote
    {
        return $this->voteProvider;
    }

    public function save(Request $request, int $id): SaveResult
    {
        $block = $this->getBlock($id);
        if (! $block->isOpen()) {
            $message = sprintf('Sorry, %s is not longer open for voting.', (string) $block);

            return $this->newVO(['saved' => false, 'flashMessages' => ['flash:danger' => $message]]);
        }
        $saved = $this->getVoteProvider()->saveVotes($id, $request->user()->id, $request->input('vote', []));

        return $this->newVO(['saved' => $saved, 'flashMessages' => []]);
    }

    protected function getBlock(int $id): Block
    {
        return $this->getBlockRetrieveService()->findById($id)->model;
    }
}
