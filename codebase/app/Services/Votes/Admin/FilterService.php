<?php

namespace App\Services\Votes\Admin;

use Illuminate\Http\Request;
use Smorken\Support\Contracts\Filter;

class FilterService extends \Smorken\Service\Services\FilterService
{
    protected function createFilterFromRequest(Request $request): Filter
    {
        return new \Smorken\Support\Filter([
            'block_id' => $request->input('block_id'),
            'aggregate' => $request->boolean('aggregate', false),
        ]);
    }
}
