<?php

namespace App\Services\Votes\Admin;

use App\Contracts\Storage\Block;
use App\Contracts\Storage\Vote;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Smorken\Service\Contracts\Services\VO\IndexResult;
use Smorken\Service\Services\BaseService;
use Smorken\Service\Services\Traits\HasFilterServiceTrait;
use Smorken\Support\Contracts\Filter;

class IndexService extends BaseService implements \App\Contracts\Services\Votes\Admin\IndexService
{
    use HasFilterServiceTrait;

    protected string $voClass = \Smorken\Service\Services\VO\IndexResult::class;

    public function __construct(protected Vote $voteProvider, protected Block $blockProvider, array $services = [])
    {
        parent::__construct($services);
    }

    public function getBlockProvider(): Block
    {
        return $this->blockProvider;
    }

    public function getByRequest(Request $request): IndexResult
    {
        $filter = $this->getFilterService()->getFilterFromRequest($request);
        $models = $this->getModels($filter);

        return $this->newVO(['models' => $models, 'filter' => $filter]);
    }

    public function getProvider(): Vote
    {
        return $this->voteProvider;
    }

    protected function getModels(Filter $filter): Collection|Paginator
    {
        if ($filter->aggregate === true) {
            return $this->getBlockProvider()->getByFilterWithCounts($filter);
        }

        return $this->getProvider()->getByFilter($filter);
    }
}
