<?php

namespace App\Services\Votes\Admin;

use Illuminate\Http\Request;
use Smorken\Export\Contracts\Export;
use Smorken\Service\Services\ExportByStorageProviderService;
use Smorken\Support\Contracts\Filter;

class ExportService extends ExportByStorageProviderService implements \App\Contracts\Services\Votes\Admin\ExportService
{
    protected function getHeaderCallback(...$params): ?callable
    {
        return fn (Export $exporter, array $headers, mixed $firstModel): array => ['block_id', 'block', 'candidate_id', 'candidate', 'type_id', 'type', 'voter', 'vote_date'];
    }

    protected function getRowCallback(...$params): ?callable
    {
        return function (Export $exporter, array $data, mixed $model): array {
            $rows = [];
            $base_row = [
                'block_id' => $model->id,
                'block' => $model->name,
            ];
            foreach ($model->candidates as $candidate) {
                $candidate_row = $base_row + [
                    'candidate_id' => $candidate->id,
                    'candidate' => $candidate->name,
                    'type_id' => $candidate->type_id,
                    'type' => $candidate->type ? $candidate->type->name : $candidate->type_id,
                ];
                foreach ($candidate->votes as $vote) {
                    $rows[] = $candidate_row + [
                        'voter' => $vote->user_id,
                        'vote_date' => $vote->updated_at,
                    ];
                }
            }

            return $rows;
        };
    }

    protected function processModels(Request $request, Filter $filter): void
    {
        $this->getProvider()->chunkWithVotes($filter, function ($models) {
            $this->handleHeader($models);
            $this->getExporter()->process($models);
        });
    }
}
