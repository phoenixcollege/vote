<?php

namespace App\Services\Votes;

use Smorken\Service\Services\VO\VOResult;

class SaveResult extends VOResult implements \App\Contracts\Services\Votes\SaveResult
{
    public function __construct(
        public bool $saved,
        public array $flashMessages
    ) {
    }
}
