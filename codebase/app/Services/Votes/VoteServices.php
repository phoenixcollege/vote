<?php

namespace App\Services\Votes;

use App\Contracts\Services\Blocks\RetrieveService;
use Smorken\Service\Services\Factory;

class VoteServices extends Factory implements \App\Contracts\Services\Votes\VoteServices
{
    protected array $services = [
        RetrieveService::class => null,
        \App\Contracts\Services\Candidates\RetrieveService::class => null,
        \App\Contracts\Services\Votes\SaveService::class => null,
    ];
}
