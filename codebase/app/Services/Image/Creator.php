<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/31/16
 * Time: 11:21 AM
 */

namespace App\Services\Image;

use Smorken\Image\Sizer\Contracts\Result;
use Smorken\Image\Sizer\Contracts\Sizer;

class Creator implements \App\Contracts\Image\Creator
{
    public function __construct(protected Sizer $sizer)
    {
    }

    public function create(string $raw): Result
    {
        return $this->sizer->size($raw);
    }
}
