<?php

namespace App\Services\Categories\Admin;

use Smorken\Service\Services\PaginateByFilterService;

class IndexService extends PaginateByFilterService implements \App\Contracts\Services\Categories\Admin\IndexService
{
}
