<?php

namespace Database\Seeders;

class TestSeeder extends DatabaseSeeder
{
    protected function getSeeders(): array
    {
        $seeders = parent::getSeeders();
        $seeders[] = TestCategoryAndTypeSeeder::class;

        return $seeders;
    }
}
