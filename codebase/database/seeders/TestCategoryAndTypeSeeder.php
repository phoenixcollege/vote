<?php

namespace Database\Seeders;

class TestCategoryAndTypeSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $category = \App\Models\Eloquent\Category::factory()->create(['name' => 'Test Category']);
        \App\Models\Eloquent\Type::factory()->create([
            'name' => 'Type 1',
            'category_id' => $category->id,
        ]);
        \App\Models\Eloquent\Type::factory()->create([
            'name' => 'Type 2',
            'category_id' => $category->id,
        ]);
    }
}
