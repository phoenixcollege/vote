<?php

namespace Database\Factories\App\Models\Eloquent;

use App\Models\Eloquent\Type;
use Illuminate\Database\Eloquent\Factories\Factory;

class TypeFactory extends Factory
{
    protected $model = Type::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->words(3, true),
            'category_id' => $this->faker->randomNumber(3),
        ];
    }
}
