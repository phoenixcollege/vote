<?php

namespace Database\Factories\App\Models\Eloquent;

use App\Models\Eloquent\Block;
use Illuminate\Database\Eloquent\Factories\Factory;

class BlockFactory extends Factory
{
    protected $model = Block::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->words(3, true),
            'category_id' => $this->faker->randomNumber(3),
            'start' => $this->faker->dateTimeBetween('-1 week', '+1 day'),
            'end' => $this->faker->dateTimeBetween('+2 days', '+1 week'),
            'private' => 0,
        ];
    }
}
