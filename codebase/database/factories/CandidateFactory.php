<?php

namespace Database\Factories\App\Models\Eloquent;

use App\Models\Eloquent\Candidate;
use Illuminate\Database\Eloquent\Factories\Factory;

class CandidateFactory extends Factory
{
    protected $model = Candidate::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'bio' => $this->faker->paragraphs(2, true),
            'title' => $this->faker->words(3, true),
            'type_id' => $this->faker->randomNumber(3),
            'block_id' => $this->faker->randomNumber(3),
        ];
    }
}
