<?php

namespace Database\Factories\App\Models\Eloquent;

use App\Models\Eloquent\Vote;
use Illuminate\Database\Eloquent\Factories\Factory;

class VoteFactory extends Factory
{
    protected $model = Vote::class;

    public function definition(): array
    {
        return [
            'user_id' => $this->faker->randomNumber(3),
            'candidate_id' => $this->faker->randomNumber(3),
        ];
    }
}
