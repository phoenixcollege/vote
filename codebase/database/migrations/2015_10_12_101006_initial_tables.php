<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blocks', function (Blueprint $t) {
            $t->increments('id');
            $t->string('name', 64);
            $t->dateTime('start');
            $t->dateTime('end');
            $t->integer('category_id')->unsigned();
            $t->boolean('private')->default(1);
            $t->timestamps();

            $t->index('start', 'b_start_ndx');
            $t->index('end', 'b_end_ndx');
            $t->index('category_id', 'b_category_id_ndx');
            $t->index('private', 'b_private_ndx');
        });

        Schema::create('categories', function (Blueprint $t) {
            $t->increments('id');
            $t->string('name', 64);
            $t->timestamps();
        });

        Schema::create('types', function (Blueprint $t) {
            $t->increments('id');
            $t->string('name', 64);
            $t->integer('category_id')->unsigned();
            $t->timestamps();

            $t->index('category_id', 't_category_id_ndx');
        });

        Schema::create('candidates', function (Blueprint $t) {
            $t->increments('id');
            $t->string('name', 64);
            $t->integer('type_id')->unsigned();
            $t->integer('block_id')->unsigned();
            $t->string('title', 64);
            $t->text('bio');
            $t->timestamps();

            $t->index('type_id', 'c_type_id_ndx');
            $t->index('block_id', 'c_block_id_ndx');
        });

        Schema::create('votes', function (Blueprint $t) {
            $t->increments('id');
            $t->string('user_id', 32);
            $t->integer('candidate_id')->unsigned();
            $t->timestamps();

            $t->index('user_id', 'v_user_id_ndx');
            $t->index('candidate_id', 'v_candidate_id_ndx');
        });

        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mime', 32);
            $table->integer('size')->unsigned();
            //            $table->binary('data');
            $table->integer('candidate_id')->unsigned();
            $table->boolean('default')->default(0);
            $table->timestamps();

            $table->index('candidate_id', 'im_candidate_id_ndx');
            $table->index('default', 'im_default_ndx');
        });

        DB::statement('ALTER TABLE images ADD data MEDIUMBLOB');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blocks');
        Schema::drop('categories');
        Schema::drop('types');
        Schema::drop('candidates');
        Schema::drop('votes');
        Schema::drop('images');
    }
};
