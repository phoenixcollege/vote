<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\HomeController::class, 'index']);
Route::post('/', [\App\Http\Controllers\HomeController::class, 'doIndex']);

Route::middleware(['auth'])
    ->group(function () {
        Route::prefix('image')
            ->group(function () {
                Route::get('view/{id}', [\App\Http\Controllers\Image\Controller::class, 'view']);
            });
        Route::prefix('vote')
            ->group(function () {
                $controller = \App\Http\Controllers\Vote\Controller::class;
                Route::get('/{id}', [$controller, 'index']);
                Route::post('/{id}', [$controller, 'doSave']);
                Route::get('view-candidate/{id}/{c_id}', [$controller, 'viewCandidate']);
            });
    });

Route::group([
    'prefix' => 'admin',
    'middleware' => ['auth', 'can:role-admin'],
    'namespace' => 'Admin',
], function () {
    Route::prefix('block')
        ->group(function () {
            \Smorken\Support\Routes::create(\App\Http\Controllers\Admin\Block\Controller::class);
        });

    Route::prefix('candidate')
        ->group(function () {
            \Smorken\Support\Routes::create(\App\Http\Controllers\Admin\Candidate\Controller::class, [], [
                'get|default-image/{id}/{image_id}' => 'defaultImage',
                'get|delete-image/{id}/{image_id}' => 'deleteImage',
            ]);
        });

    Route::prefix('category')
        ->group(function () {
            \Smorken\Support\Routes::create(\App\Http\Controllers\Admin\Category\Controller::class);
        });

    Route::prefix('type')
        ->group(function () {
            \Smorken\Support\Routes::create(\App\Http\Controllers\Admin\Type\Controller::class);
        });

    Route::prefix('vote')
        ->group(function () {
            $controller = \App\Http\Controllers\Admin\Vote\Controller::class;
            Route::get('/', [$controller, 'index']);
            Route::get('/export', [$controller, 'export']);
            Route::get('/delete/{id}', [$controller, 'delete']);
            Route::delete('/delete/{id}', [$controller, 'doDelete']);
        });
});
