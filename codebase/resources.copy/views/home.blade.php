@extends('layouts.app')
@section('content')
    <div class="py-1">
        @auth
            @if ($blocks && count($blocks))
                <form method="post" class="card card-body">
                    @include('_preset.input.g_select', [
                    'name' => 'id',
                    'title' => 'Pick one',
                    'items' => $blocks->pluck('name', 'id')->all()
                    ])
                    @include('_preset.input._button', ['title' => 'Go', 'classes' => 'btn-success'])
                </form>
            @else
                <div class="text-muted">There are no voting blocks currently available.</div>
            @endif
        @else
            You must <a href="{{ route('login') }}" title="Log in">log in</a> to vote.
        @endauth
    </div>
    @foreach($charts as $block_id => $block_charts)
        <div class="card">
            <div class="card-header"><h5 class="card-title">{{ $block_charts['block']->name }}</h5></div>
            <div class="card-body">
                <div class="row">
                    @foreach($block_charts['charts'] as $i => $chart)
                        <div class="col-md">
                            <div id="chart-{{ $i }}"></div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endforeach
@endsection
@push('add_to_end')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

        // Load the Visualization API and the corechart package.
        google.charts.load('current', {'packages': ['corechart']});

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawCharts);

        function drawCharts() {
            @foreach ($charts as $block_id => $block_charts)
            @if ($block_charts['charts'] && count($block_charts['charts']))
            @foreach ($block_charts['charts'] as $i => $chart)
            drawChart({{ $i }}, {!! json_encode($chart) !!});
            @endforeach
            @endif
            @endforeach
        }

        // Callback that creates and populates a data table,
        // instantiates the pie chart, passes in the data and
        // draws it.
        function drawChart(i, chartdata) {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Candidate');
            data.addColumn('number', 'Votes');
            var options = {
                'title': chartdata.name,
                'width': '100%'
            };
            var ds = chartdata.datasets;
            for (var name in ds) {
                if (ds.hasOwnProperty(name)) {
                    data.addRow([name, ds[name]]);
                }
            }
            var chart = new google.visualization.PieChart(document.getElementById('chart-' + i));
            chart.draw(data, options);
        }
    </script>
@endpush
