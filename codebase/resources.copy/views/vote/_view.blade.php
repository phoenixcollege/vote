<?php $category = $block->category; ?>
<div class="mb-2">
    @include('_preset.input._anchor', ['title' => 'Back to Home', 'href' => action([\App\Http\Controllers\HomeController::class, 'index'])])
</div>
@if ($category && $category->types && count($category->types))
    @foreach($category->types as $type)
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{ $type->name }}</h3>
            </div>
            <div class="card-body">
                <?php $candidates = $block->candidates->where('type_id', $type->id); ?>
                @if ($candidates && count($candidates))
                    @foreach($candidates as $candidate)
                        <?php $voted_for = count($candidate->votes->where('candidate_id', $candidate->id)); ?>
                        @include(
                            'vote._candidate_short',
                            [
                                'candidate' => $candidate,
                                'voted_for' => $voted_for,
                                'type_id' => $type->id,
                                'open' => $open
                            ]
                        )
                    @endforeach
                @else
                    <div class="text-muted">No candidates found.</div>
                @endif
            </div>
        </div>
    @endforeach
@else
    <div class="text-muted">No vote-able types found.</div>
@endif
