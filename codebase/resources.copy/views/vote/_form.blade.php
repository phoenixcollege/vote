<form method="post" action="{{ action([$controller, 'doSave'], ['id' => $block->id]) }}">
    @php $category = $block->category; @endphp
    @csrf
    @if ($category && $category->types && count($category->types))
        @foreach($category->types as $type)
            <div class="card mb-2">
                <div class="card-header bg-primary text-white">
                    <h3 class="card-title">{{ $type->name }}</h3>
                </div>
                <div>
                    <?php $candidates = $block->candidates->where('type_id', $type->id); ?>
                    @if ($candidates && count($candidates))
                        @foreach($candidates as $candidate)
                            <?php $voted_for = count($candidate->votes->where('candidate_id', $candidate->id)); ?>
                            @include(
                                'vote._candidate_short',
                                [
                                    'candidate' => $candidate,
                                    'voted_for' => $voted_for,
                                    'type_id' => $type->id,
                                    'open' => $open
                                ]
                            )
                        @endforeach
                    @else
                        <div class="text-muted">No candidates found.</div>
                    @endif
                </div>
            </div>
        @endforeach
        <div class="">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="{{ action([\App\Http\Controllers\HomeController::class, 'index']) }}"
               title="Cancel"
               class="btn btn-outline-secondary">Cancel</a>
        </div>
    @else
        <div class="text-muted">No vote-able types found.</div>
    @endif
</form>
