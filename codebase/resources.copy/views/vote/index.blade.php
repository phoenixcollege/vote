@extends('layouts.app')
@section('content')
    @php $open = $block->isOpen(); @endphp
    <h3>
        {{ $block->name }}
    </h3>
    <div>
        {{ \Carbon\Carbon::parse($block->start)->toDayDateTimeString() }} through {{ \Carbon\Carbon::parse($block->end)->toDayDateTimeString() }}
        @if ($open)
            <span class="text-success">(open)</span>
        @else
            <span class="text-danger">(closed)</span>
        @endif
    </div>
    @if ($open)
        <div class="alert alert-info">Don't forget to click 'Save' when you are finished voting!</div>
        @include('vote._form')
    @else
        @include('vote._view')
    @endif
@endsection
