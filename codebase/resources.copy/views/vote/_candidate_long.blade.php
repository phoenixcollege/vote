<div class="card">
    <div class="card-header">
        <h3 class="card-title">{!! $sanitize->xss($candidate->name) !!}</h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md">
                @if ($candidate->image)
                    <img
                        src="{{ action([\App\Http\Controllers\Image\Controller::class, 'view'], ['id' => $candidate->image->id]) }}"
                        alt="Photo" style="max-height: 400px; max-width: 400px" class="image-responsive">
                @else
                    <img src="{{ asset('images/default.png') }}"
                         alt="Photo" style="max-height: 400px; max-width: 400px" class="image-responsive">
                @endif
            </div>
            <div class="col-md">
                <h4>
                    {!! $sanitize->xss($candidate->title) !!}
                </h4>
                <p>
                    {!! nl2br($sanitize->xss($candidate->bio)) !!}
                </p>
            </div>
        </div>
    </div>
</div>

