<div class="card mt-2">
    <div class="card-header">
        <div class="row">
            <div class="col-1">
                @if ($open)
                    <input type="radio" id="vote-{{ $type_id }}-{{ $candidate->id }}" name="vote[{{ $type_id }}]"
                           value="{{ $candidate->id }}" {{ $voted_for ? 'checked' : null }} class="radio">
                @else
                    @if($voted_for)
                        <span class="bi bi-check-square text-success"><span class="visually-hidden">Voted for</span></span>
                    @endif
                @endif
            </div>
            <div class="col">
                <h4 class="card-title">
                    <label for="vote-{{ $type_id }}-{{ $candidate->id }}">
                        {!! $sanitize->xss($candidate->name) !!}
                    </label>
                </h4>
                <h5>{!! $sanitize->xss($candidate->title) !!}</h5>
            </div>
            <div class="col-1">
                @include('_preset.input._anchor', [
                'title' => 'View...',
                'href' => action([$controller, 'viewCandidate'],['id' => $block->id, 'c_id' => $candidate->id]),
                'classes' => 'btn btn-success btn-sm pull-right'
                ])
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-9">
                {!! nl2br($sanitize->xss($candidate->bio)) !!}
            </div>
            <div class="hidden-xs col-xm-4 col-md-3">
                @if ($candidate->image)
                    <img
                        src="{{ action([\App\Http\Controllers\Image\Controller::class, 'view'], ['id' => $candidate->image->id]) }}"
                        alt="Photo" style="max-height: 200px; max-width: 200px" class="image-responsive">
                @else
                    <img src="{{ asset('images/default.png') }}"
                         alt="Photo" style="max-height: 200px; max-width: 200px" class="image-responsive">
                @endif
            </div>
        </div>
    </div>
</div>
