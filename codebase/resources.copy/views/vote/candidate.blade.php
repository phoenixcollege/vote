@extends('layouts.app')
@section('content')
    <div class="py-1">
        @include('_preset.input._anchor', [
        'title' => 'Back',
        'href' => action([$controller, 'index'], ['id' => $block_id]),
        'classes' => 'btn btn-large btn-primary'
        ])
    </div>
    @include('vote._candidate_long')
@endsection
