<?php
/**
 * @var \Illuminate\Support\Collection $models
 */
?>
@extends('layouts.app')
@section('content')
    @include('_preset.controller._title', ['title' => 'Candidate Administration'])
    @includeIf('admin.candidate._filter_form')
    @include('_preset.controller.index_actions._create')
    <div class="text-muted">Updating and creating will be unavailable without active or upcoming voting blocks.</div>
    @if ($models && count($models))
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Candidate</th>
                <th>Block</th>
                <th>Type</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($models as $model)
                <?php $params = array_merge([$model->getKeyName() => $model->getKey()],
                    isset($filter) ? $filter->except(['page']) : []); ?>
                <tr>
                    <td>@include('_preset.controller.index_actions._view', ['value' => $model->getKey()])</td>
                    <td>{{ $model->name }}</td>
                    <td>{{ $model->block ? $model->block->name : $model->block_id }}</td>
                    <td>{{ $model->type ? $model->type->name : $model->type_id }}</td>
                    <td>
                        @include('_preset.controller.index_actions._update_delete')
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if (method_exists($models, 'links'))
            {{ $models->appends(isset($filter)?$filter->except(['page']):[])->links() }}
        @endif
    @else
        <div class="text-muted">No records found.</div>
    @endif
@endsection
