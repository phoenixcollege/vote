@extends('layouts.app')
<?php
$params = array_merge([$model->getKeyName() => $model->getKey()], isset($filter) ? $filter->all() : []);
$first_col = 'col-sm-2';
$second_col = 'col-sm-10';
?>
@section('content')
    @include('_preset.controller._to_index')
    @include('_preset.controller._title', ['title' => 'Candidate Administration'])
    <h5 class="mb-2">View record [{{ $model->getKey() }}]</h5>
    <div class="row mb-4">
        <div class="col">
            <a href="{{ action([$controller, 'update'], $params) }}" title="Update {{ $model->getKey() }}"
               class="btn btn-primary btn-block w-100">Update</a>
        </div>
        <div class="col">
            <a href="{{ action([$controller, 'delete'], $params) }}" title="Delete {{ $model->getKey() }}"
               class="btn btn-danger btn-block w-100">Delete</a>
        </div>
    </div>
    <div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">ID</div>
            <div class="{{ $second_col }}">{{ $model->id }}</div>
        </div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">Candidate</div>
            <div class="{{ $second_col }}">{!! $sanitize->xss($model->name) !!}</div>
        </div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">Block</div>
            <div class="{{ $second_col }}">{{ $model->block ? $model->block->name : $model->block_id }}</div>
        </div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">Type</div>
            <div class="{{ $second_col }}">{{ $model->type ? $model->type->name : $model->type_id }}</div>
        </div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">Title</div>
            <div class="{{ $second_col }}">{!! $sanitize->xss($model->title) !!}</div>
        </div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">Bio</div>
            <div class="{{ $second_col }}">{!! nl2br($sanitize->xss($model->bio)) !!}</div>
        </div>
    </div>
    @if ($model->images && count($model->images))
        @foreach($model->images as $image)
            <div class="card mb-2">
                <div class="card-header">
                    Image #{{ $image->id }}
                    <span class="pull-right float-end">
                        @if (!$image->default)
                            @include('_preset.input._anchor', [
                                'href' => action([$controller, 'defaultImage'], array_replace($params, ['id' => $model->id, 'image_id' => $image->id])),
                                'title' => 'Default image for ' . $model->name,
                                'classes' => 'text-success',
                            ])
                        @else
                            <span class="text-muted">Default</span>
                        @endif
                        &nbsp;|&nbsp;
                        @include('_preset.input._anchor', [
                        'href' => action([$controller, 'deleteImage'], array_replace($params, ['id' => $model->id, 'image_id' => $image->id])),
                        'title' => 'Delete image for ' . $model->name,
                        'classes' => 'text-danger',
                        ])
                    </span>
                </div>
                <div class="card-body">
                    <img
                        src="{{ action([\App\Http\Controllers\Image\Controller::class, 'view'], ['id' => $image->id]) }}"
                        class="rounded img-fluid" alt="Photo for {{ $model->name }}">
                    @if ($image->default)
                        <div class="image-default">Default</div>
                    @endif
                </div>
            </div>
        @endforeach
    @endif
@endsection

