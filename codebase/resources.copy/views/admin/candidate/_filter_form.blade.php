<?php $filtered = 'border border-success'; ?>
<div class="card my-2">
    <div class="card-body">
        <form class="row g-3 align-items-center" method="get">
            <div class="col-md">
                <label for="block_id" class="form-label visually-hidden">Block</label>
                <select name="block_id" class="form-select">
                    <option value="">All Blocks</option>
                    @foreach ($blocks as $block)
                        <option
                                value="{{ $block->id }}" {{ $filter->block_id && $filter->block_id == $block->id ? 'selected' : ''}}>
                            {{ $block->name }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md">
                <button type="submit" class="btn btn-primary mb-2 mr-2">Filter</button>
                <a href="{{ action([$controller, 'index']) }}" class="btn btn-outline-danger mb-2" title="Reset filter">Reset</a>
            </div>
        </form>
    </div>
</div>
