@include('_preset.input.g_input', ['name' => 'name', 'title' => "Candidate's Name"])
@include('_preset.input.g_select', [
'name' => 'block_id',
'title' => 'Block',
'items' => $blocks->pluck('name', 'id')->all()
])
@include('_preset.input.g_select', [
'name' => 'type_id',
'title' => 'Type',
'items' => $types->pluck('name', 'id')->all()
])
@include('_preset.input.g_input', ['name' => 'title', 'title' => 'Brief Description/Title'])
@include('_preset.input.g_textarea', ['name' => 'bio', 'title' => 'Bio'])
<div class="form-group mb-2">
    @include('_preset.input._label', ['name' => 'photo', 'title' => 'Picture'])
    <input type="file" name="photo" class="form-control" accept="image/*">
</div>
@if ($model->images && count($model->images))
    <div class="card mb-w">
        <div class="row">
            @foreach ($model->images as $image)
                <div class="col-md-4">
                    <img
                        src="{{ action([\App\Http\Controllers\Image\Controller::class, 'view'], ['id' => $image->id]) }}"
                        alt="Photo for {{ $model->name }}">
                    @if ($image->default)
                        <div class="image-default">Default</div>
                    @endif
                </div>
            @endforeach
        </div>
    </div>
@endif
