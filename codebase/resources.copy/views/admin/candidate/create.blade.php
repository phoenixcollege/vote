@extends('layouts.app')
<?php
$params = array_merge([$model->getKeyName() => $model->getKey()], isset($filter) ? $filter->all() : []);
?>
@section('content')
    @include('_preset.controller._to_index')
    @include('_preset.controller._title', ['title' => 'Candidate Administration'])
    <h5 class="mb-2">Create new record</h5>
    @if ($blocks && count($blocks))
        <form method="post" enctype="multipart/form-data" action="{{ action([$controller, 'doSave'], $params) }}">
            @csrf
            <div class="mb-4">
                @include('admin.candidate._form')
            </div>
            <div class="">
                <button type="submit" class="btn btn-primary">Save</button>
                <a href="{{ action([$controller, 'index'], $params) }}" title="Cancel"
                   class="btn btn-outline-secondary">Cancel</a>
            </div>
        </form>
    @else
        <div class="text-muted">There are no voting blocks active or upcoming.</div>
    @endif
@endsection
