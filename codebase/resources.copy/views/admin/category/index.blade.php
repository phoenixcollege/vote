<?php
/**
 * @var \Illuminate\Support\Collection $models
 */
?>
@extends('layouts.app')
@include('_preset.controller.index', [
'title' => 'Category Administration',
'filter_form_view' => 'admin.category._filter_form'
])
