<?php $filtered = 'border border-success'; ?>
<div class="card mt-2 mb-2">
    <div class="card-body">
        <form class="row g-3 align-items-center" method="get">
            <div class="col-md">
                <label for="category_id" class="form-label visually-hidden">Category</label>
                <select name="category_id" class="form-select">
                    <option value="">All Categories</option>
                    @foreach ($categories as $category)
                        <option
                                value="{{ $category->id }}" {{ $filter->category_id && $filter->category_id == $category->id ? 'selected' : ''}}>
                            {{ $category->name }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md">
                <button type="submit" class="btn btn-primary mr-2">Filter</button>
                <a href="{{ action([$controller, 'index']) }}" class="btn btn-outline-danger"
                   title="Reset filter">Reset</a>
            </div>
        </form>
    </div>
</div>
