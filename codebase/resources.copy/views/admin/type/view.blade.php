@extends('layouts.app')
@include('_preset.controller.view', [
'title' => 'Type Administration',
'extra' => ['Category' => $model->category ? $model->category->name : $model->category_id]
])
