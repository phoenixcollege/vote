@include('_preset.input.g_input', ['name' => 'name', 'title' => 'Type'])
@include('_preset.input.g_select', [
'name' => 'category_id',
'title' => 'Category',
'items' => ['' => 'Select one'] + $categories->pluck('name', 'id')->all()
])
