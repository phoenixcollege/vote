@extends('layouts.app')
@include('_preset.controller.view', [
'title' => 'Block Administration',
'extra' => ['Category' => $model->category ? $model->category->name : $model->category_id]
])
@section('content')
    <p>
        <strong>Link to vote: </strong>
        {{ action([\App\Http\Controllers\Vote\Controller::class, 'index'], ['id' => $model->id]) }}
    </p>
    <div class="card">
        <div class="card-header">Results</div>
        <table class="table table-striped">
            <thead>
            <th>Type</th>
            <th>Candidate</th>
            <th>Count</th>
            </thead>
            <tbody>
            @php $grand_total = 0; @endphp
            @foreach($model->candidates as $candidate)
                @php $vote_count = (int)count($candidate->votes); @endphp
                <tr>
                    <td>{{ $candidate->type ? $candidate->type->name : $candidate->type_id }}</td>
                    <td>{{ $candidate->name }}</td>
                    <td>{{ $vote_count }}</td>
                    @php $grand_total += $vote_count; @endphp
                </tr>
            @endforeach
            <tr class="info">
                <td colspan="2">Total</td>
                <td>{{ $grand_total }}</td>
            </tr>
            </tbody>
        </table>
    </div>
@append
