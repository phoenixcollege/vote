@include('_preset.input.g_input', ['name' => 'name', 'title' => 'Block'])
<div class="form-group mb-2">
    @include('_preset.input._label', ['name' => 'start', 'title' => 'Start Date'])
    <input type="datetime-local" class="form-control" name="start" id="start" value="{{ old('start', $model->toDateTimeLocalFormat('start')) }}">
</div>
<div class="form-group mb-2">
    @include('_preset.input._label', ['name' => 'end', 'title' => 'End Date'])
    <input type="datetime-local" class="form-control" name="end" id="end" value="{{ old('end', $model->toDateTimeLocalFormat('end')) }}">
</div>
@include('_preset.input.g_select', [
'name' => 'category_id',
'title' => 'Category',
'items' => ['' => 'Select one'] + $categories->pluck('name', 'id')->all()
])
@include('_preset.input.g_check_bool', ['name' => 'private', 'title' => 'Hide from landing page'])
