<?php
/**
 * @var \Illuminate\Support\Collection $models
 */
?>
@extends('layouts.app')
@section('content')
    @include('_preset.controller._title', ['title' => 'Block Administration'])
    @includeIf('admin.block._filter_form')
    @include('_preset.controller.index_actions._create')

    @if ($models && count($models))
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Block</th>
                <th>Start</th>
                <th>End</th>
                <th>Hidden</th>
                <th>Open</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($models as $model)
                <?php $params = array_merge([$model->getKeyName() => $model->getKey()],
                    isset($filter) ? $filter->except(['page']) : []); ?>
                <tr>
                    <td>@include('_preset.controller.index_actions._view', ['value' => $model->getKey()])</td>
                    <td>{{ $model->name }}</td>
                    <td>{{ $model->start }}</td>
                    <td>{{ $model->end }}</td>
                    <td>{{ $model->private ? 'Yes' : 'No' }}</td>
                    <td>{{ $model->isOpen() ? 'Yes' : 'No' }}</td>
                    <td>
                        @include('_preset.controller.index_actions._update_delete')
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if (method_exists($models, 'links'))
            {{ $models->appends(isset($filter)?$filter->except(['page']):[])->links() }}
        @endif
    @else
        <div class="text-muted">No records found.</div>
    @endif
@endsection
