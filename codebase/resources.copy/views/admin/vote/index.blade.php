<?php
/**
 * @var \Illuminate\Support\Collection $models
 */
?>
@extends('layouts.app')
@section('content')
    @include('_preset.controller._title', ['title' => 'Vote Administration'])
    @includeIf('admin.vote._filter_form')

    @if ($models && count($models))
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Voter</th>
                <th>Candidate</th>
                <th>Type</th>
                <th>Date</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @php $last_block = null; $colspan = 6; @endphp
            @foreach ($models as $model)
                @php
                    $candidate = $model->candidate;
                    $block = $candidate && $candidate->block ? $candidate->block : null;
                @endphp
                @if (is_null($last_block) || ($last_block && $block->id !== $last_block->id))
                    <tr class="block-header">
                        <td class="bg-primary text-white" colspan="{{ $colspan }}">{{ $block->name }}</td>
                    </tr>
                    @php $last_block = $block; @endphp
                @endif
                <?php $params = array_merge([$model->getKeyName() => $model->getKey()],
                    isset($filter) ? $filter->except(['page']) : []); ?>
                <tr>
                    <td>{{ $model->getKey() }}</td>
                    <td>{{ substr($model->user_id, -10) }}</td>
                    <td>{{ $candidate->name }}</td>
                    <td>{{ $candidate->type ? $candidate->type->name : $candidate->type_id }}</td>
                    <td>{{ $model->updated_at }}</td>
                    <td>
                        @include('_preset.controller.index_actions._delete')
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if (method_exists($models, 'links'))
            {{ $models->appends(isset($filter)?$filter->except(['page']):[])->links() }}
        @endif
    @else
        <div class="text-muted">No records found.</div>
    @endif
@endsection
