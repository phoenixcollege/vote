<?php $filtered = 'border border-success'; ?>
<div class="card mt-2 mb-2">
    <div class="card-body">
        <form class="row g-3 align-items-center" method="get">
            <div class="col-md">
                <label for="block_id" class="visually-hidden">Block</label>
                <select name="block_id" id="block_id" class="form-select">
                    <option value="">All Blocks</option>
                    @foreach ($blocks as $block)
                        <option
                                value="{{ $block->id }}" {{ $filter->block_id && $filter->block_id == $block->id ? 'selected' : ''}}>
                            {{ $block->name }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md">
                <input class="form-check-input" type="checkbox" id="aggregate" value="1"
                       name="aggregate" {{ $filter->aggregate ? 'checked' : null }}>
                <label class="form-check-label" for="aggregate">Aggregate</label>
            </div>
            <div class="col-md">
                <button type="submit" class="btn btn-primary mr-2">Filter</button>
                <a href="{{ action([$controller, 'index']) }}" class="btn btn-outline-danger"
                   title="Reset filter">Reset</a>
            </div>
        </form>
        <div class="mt-2">
            @include('_preset.input._anchor', ['title' => 'Export', 'href' => action([$controller, 'export'], $filter->toArray()), 'classes' => 'btn btn-success'])
            <small>Filter your results <i>before</i> exporting.</small>
        </div>
    </div>
</div>
