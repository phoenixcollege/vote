<?php
/**
 * @var \Illuminate\Support\Collection $models
 */
?>
@extends('layouts.app')
@section('content')
    @include('_preset.controller._title', ['title' => 'Vote Administration'])
    @includeIf('admin.vote._filter_form')

    @if ($models && count($models))
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Candidate</th>
                <th>Type</th>
                <th>Count</th>
            </tr>
            </thead>
            <tbody>
            @php $colspan = 3; @endphp
            @foreach ($models as $block)
                <tr class="block-header">
                    <td class="bg-primary text-white" colspan="{{ $colspan }}">{{ $block->name }}</td>
                </tr>
                @foreach ($block->candidates as $candidate)
                    <tr>
                        <td>{{ $candidate->name }}</td>
                        <td>{{ $candidate->type ? $candidate->type->name : $candidate->type_id }}</td>
                        <td>{{ $candidate->votes_count }}</td>
                    </tr>
                @endforeach
            @endforeach
            </tbody>
        </table>
        @if (method_exists($models, 'links'))
            {{ $models->appends(isset($filter)?$filter->except(['page']):[])->links() }}
        @endif
    @else
        <div class="text-muted">No records found.</div>
    @endif
@endsection
