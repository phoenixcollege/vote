<?php

return [
    'concrete' => [
        \App\Storage\Eloquent\Block::class => [
            'model' => [
                'impl' => \App\Models\Eloquent\Block::class,
            ],
            'cacheAssistOptions' => [
                'forgetAuto' => [['activeupcoming'], ['current']],
            ],
        ],
        \App\Storage\Eloquent\Candidate::class => [
            'model' => [
                'impl' => \App\Models\Eloquent\Candidate::class,
            ],
        ],
        \App\Storage\Eloquent\Category::class => [
            'model' => [
                'impl' => \App\Models\Eloquent\Category::class,
            ],
        ],
        \App\Storage\Eloquent\Image::class => [
            'model' => [
                'impl' => \App\Models\Eloquent\Image::class,
            ],
        ],
        \App\Storage\Eloquent\Type::class => [
            'model' => [
                'impl' => \App\Models\Eloquent\Type::class,
            ],
        ],
        \App\Storage\Eloquent\Vote::class => [
            'model' => [
                'impl' => \App\Models\Eloquent\Vote::class,
            ],
        ],
    ],
    'contract' => [
        \App\Contracts\Storage\Block::class => \App\Storage\Eloquent\Block::class,
        \App\Contracts\Storage\Candidate::class => \App\Storage\Eloquent\Candidate::class,
        \App\Contracts\Storage\Category::class => \App\Storage\Eloquent\Category::class,
        \App\Contracts\Storage\Image::class => \App\Storage\Eloquent\Image::class,
        \App\Contracts\Storage\Type::class => \App\Storage\Eloquent\Type::class,
        \App\Contracts\Storage\Vote::class => \App\Storage\Eloquent\Vote::class,
    ],
];
