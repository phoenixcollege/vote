<?php

return [
    'guest' => [],
    'auth' => [],

    'role-manage' => [],
    'role-admin' => [
        [
            'name' => 'Votes',
            'action' => [\App\Http\Controllers\Admin\Vote\Controller::class, 'index'],
            'children' => [],
        ],
        [
            'name' => 'Candidates',
            'action' => [\App\Http\Controllers\Admin\Candidate\Controller::class, 'index'],
            'children' => [],
        ],
        [
            'name' => 'Blocks',
            'action' => [\App\Http\Controllers\Admin\Block\Controller::class, 'index'],
            'children' => [],
        ],
        [
            'name' => 'Types',
            'action' => [\App\Http\Controllers\Admin\Type\Controller::class, 'index'],
            'children' => [],
        ],
        [
            'name' => 'Categories',
            'action' => [\App\Http\Controllers\Admin\Category\Controller::class, 'index'],
            'children' => [],
        ],
        [
            'name' => 'Users',
            'action' => [\Smorken\SocialAuth\Http\Controllers\Admin\Controller::class, 'index'],
            'children' => [],
        ],
    ],
];
