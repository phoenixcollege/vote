<?php

return [
    'autoload' => env('SERVICES_AUTOLOAD', false),
    'path' => env('SERVICES_INVOKABLE_PATH', 'Providers/Services'),
    'invokables' => [
        \App\Providers\Services\BlockServices::class,
        \App\Providers\Services\CandidateServices::class,
        \App\Providers\Services\ImageServices::class,
        \App\Providers\Services\VoteServices::class,
        \App\Providers\Services\Admin\BlockServices::class,
        \App\Providers\Services\Admin\CandidateServices::class,
        \App\Providers\Services\Admin\CategoryServices::class,
        \App\Providers\Services\Admin\ImageServices::class,
        \App\Providers\Services\Admin\TypeServices::class,
        \App\Providers\Services\Admin\VoteServices::class,
    ],
];
